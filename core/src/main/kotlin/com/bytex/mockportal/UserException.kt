package com.bytex.mockportal

/**
 * Represents all user-related exceptions.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
abstract class UserException(val login: String, message:String, cause: Throwable? = null): MockPortalException(message, cause)
package com.bytex.mockportal.util.conversion

import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.deserialize
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.toJSON
import com.fasterxml.jackson.databind.JsonNode
import org.springframework.core.serializer.support.SerializationFailedException
import kotlin.reflect.full.cast

/**
 * Marker interface with default implementation of serialization based on JSON.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
interface JsonConvertible: Convertible {
    override fun supportsConversion(format: ConversionFormat<*>) = format === ConversionFormat.JSON

    override fun <F : Any> convertTo(format: ConversionFormat<F>): F = when (format) {
        ConversionFormat.JSON -> format.nativeType.cast(toJSON())
        else -> throw SerializationFailedException("Format $format is not supported")
    }

    override fun <F : Any> convertFrom(format: ConversionFormat<F>, input: F) = when(format) {
        ConversionFormat.JSON -> input as JsonNode deserialize this
        else -> throw SerializationFailedException("Format $format is not supported")
    }
}
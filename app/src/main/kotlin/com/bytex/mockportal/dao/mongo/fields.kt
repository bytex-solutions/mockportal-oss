package com.bytex.mockportal.dao.mongo

import com.bytex.mockportal.dao.MissingFieldException
import com.bytex.mockportal.dao.UnexpectedDataTypeException
import org.bson.Document
import org.bson.types.Binary
import org.bson.types.ObjectId
import java.io.Serializable
import java.nio.ByteBuffer
import java.util.*
import kotlin.reflect.KClass

/**
 * Represents single document field.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal sealed class DocumentField<V>(val name: String) {
    /**
     * Converts value from document.
     */
    protected abstract fun readValue(fieldValue: Any): V

    fun readValue(document: Document): V = readValue(document[name] ?: throw MissingFieldException(name))

    override fun hashCode(): Int = Objects.hash(this.javaClass, name)

    override fun toString(): String = name

    override fun equals(other: Any?): Boolean = when (other) {
        this === other -> true
        is DocumentField<*> -> javaClass.isInstance(other) && name == other.name
        else -> false
    }

    /**
     * Represents nested field.
     */
    private class Nested<T>(val parent: DocumentField<*>, val current: DocumentField<T>) : DocumentField<T>(parent.name + "." + current.name) {
        override fun readValue(fieldValue: Any): T = current.readValue(fieldValue)
        override fun hashCode(): Int = Objects.hash(parent, current)
        override fun equals(other: Any?): Boolean = other is Nested<*> && parent == other.parent && current == other.parent
    }

    /**
     * Creates a reference to nested field.
     */
    infix fun <S> nested(field: DocumentField<S>): DocumentField<S> = Nested(this, field)
}

internal sealed class ScalarField<V: Comparable<V>>(name: String, protected val fieldType: KClass<V>): DocumentField<V>(name){
    protected fun unexpectedDataType(actualType: KClass<*>): UnexpectedDataTypeException = UnexpectedDataTypeException(name, fieldType, actualType)
}

internal open class ObjectIdField(name: String): ScalarField<ObjectId>(name, ObjectId::class) {
    override fun readValue(fieldValue: Any): ObjectId =
            when (fieldValue) {
                is ObjectId -> fieldValue
                is ByteArray -> ObjectId(fieldValue)
                is ByteBuffer -> ObjectId(fieldValue)
                is MongoIdentifier -> fieldValue.value
                else -> throw unexpectedDataType(fieldValue::class)
            }
}

internal object IdField: ObjectIdField("_id")

internal open class LongField(name: String): ScalarField<Long>(name, Long::class) {
    override fun readValue(fieldValue: Any): Long =
            when (fieldValue) {
                is Long -> fieldValue
                is String -> fieldValue.toLong()
                is Number -> fieldValue.toLong()
                else -> throw unexpectedDataType(fieldValue::class)
            }
}

internal open class StringField(name: String): ScalarField<String>(name, String::class) {
    override fun readValue(fieldValue: Any): String = fieldValue.toString()
}

internal open class BooleanField(name: String): ScalarField<Boolean>(name, Boolean::class){
    override fun readValue(fieldValue: Any): Boolean =
        when(fieldValue){
            is Boolean -> fieldValue
            is String -> fieldValue.toBoolean()
            is Long, Int, Short, Byte -> fieldValue != 0
            else -> throw unexpectedDataType(fieldValue::class)
        }
}

internal open class BinaryField(name: String): DocumentField<Binary>(name) {
    override fun readValue(fieldValue: Any): Binary =
            when (fieldValue) {
                is ByteArray -> Binary(fieldValue)
                is Binary -> fieldValue
                else -> throw UnexpectedDataTypeException(name, ByteArray::class, fieldValue::class)
            }
}

internal open class ObjectField(name: String): DocumentField<Document>(name){
    override fun readValue(fieldValue: Any): Document =
            when(fieldValue){
                is Document -> fieldValue
                else -> throw UnexpectedDataTypeException(name, Document::class, fieldValue::class)
            }
}

class Variant private constructor(val value: Serializable) {
    constructor(value: Boolean) : this(value as Serializable)
    constructor(value: String) : this(value as Serializable)
    constructor(value: Document) : this(value as Serializable)
    constructor(value: Number) : this(value as Serializable)
    constructor(value: ObjectId) : this(value as Serializable)
    constructor(value: Binary) : this(value as Serializable)
}

internal class VariantField(name: String): DocumentField<Variant>(name) {
    override fun readValue(fieldValue: Any) = when (fieldValue) {
        is Boolean -> Variant(fieldValue)
        is String -> Variant(fieldValue)
        is Number -> Variant(fieldValue)
        is Document -> Variant(fieldValue)
        is ObjectId -> Variant(fieldValue)
        is Binary -> Variant(fieldValue)
        else -> throw UnexpectedDataTypeException(name, Variant::class, fieldValue::class)
    }
}
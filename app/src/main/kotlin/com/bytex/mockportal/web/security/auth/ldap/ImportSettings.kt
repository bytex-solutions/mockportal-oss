package com.bytex.mockportal.web.security.auth.ldap

import com.bytex.mockportal.UserRole
import org.springframework.boot.context.properties.NestedConfigurationProperty

/**
 * Represents LDAP import settings.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
class ImportSettings {
    /**
     * Represents mapping between group name and user role.
     */
    class UserRoleMapping : HashMap<String, UserRole>()

    /**
     * LDAP attribute which holds display name of the user.
     */
    @NestedConfigurationProperty
    var displayNameAttribute = "displayName"
    
    /**
     * Name of attribute which holds role of the user in MockPortal.
     */
    @NestedConfigurationProperty
    var roleAttribute = "mockportalRole"

    @NestedConfigurationProperty
    val roleMapping = UserRoleMapping()
}
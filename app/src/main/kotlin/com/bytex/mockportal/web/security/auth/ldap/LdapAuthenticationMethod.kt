package com.bytex.mockportal.web.security.auth.ldap

import org.springframework.ldap.core.support.BaseLdapPathContextSource
import org.springframework.security.ldap.authentication.AbstractLdapAuthenticator
import org.springframework.security.ldap.authentication.BindAuthenticator
import org.springframework.security.ldap.authentication.PasswordComparisonAuthenticator

/**
 * Represents LDAP authentication method.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal enum class LdapAuthenticationMethod {
    /**
     * Authenticate using BIND request.
     */
    BIND {
        override fun createAuthenticator(source: BaseLdapPathContextSource) = BindAuthenticator(source)
    },
    /**
     * Compares the login password with the value stored in the
     * directory using a remote LDAP "compare" operation.
     */
    PASSWORD {
        override fun createAuthenticator(source: BaseLdapPathContextSource) = PasswordComparisonAuthenticator(source)
    };

    abstract fun createAuthenticator(source: BaseLdapPathContextSource): AbstractLdapAuthenticator
}
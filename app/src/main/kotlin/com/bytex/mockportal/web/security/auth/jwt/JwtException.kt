package com.bytex.mockportal.web.security.auth.jwt

import org.springframework.security.core.AuthenticationException

/**
 * Represents exception related to wrong JWT token.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal sealed class JwtException(message: String, cause: Throwable? = null): AuthenticationException(message, cause)

internal class MissingTokenException: JwtException("JWT token is not presented in HTTP request")

internal class BadTokenException(token: String, cause: Throwable? = null): JwtException("Token $token is broken", cause)
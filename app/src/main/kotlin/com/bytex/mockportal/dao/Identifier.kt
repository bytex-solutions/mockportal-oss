package com.bytex.mockportal.dao

/**
 * Represents database-agnostic identifier of the entity.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal interface Identifier {
    override fun hashCode(): Int
    override fun equals(other: Any?): Boolean
    override fun toString(): String
}
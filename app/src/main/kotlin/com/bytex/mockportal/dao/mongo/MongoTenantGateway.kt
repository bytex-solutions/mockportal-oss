package com.bytex.mockportal.dao.mongo

import com.bytex.mockportal.dao.*
import com.bytex.mockportal.util.ifNull
import com.bytex.mockportal.util.conversion.Convertible
import com.mongodb.DuplicateKeyException
import com.mongodb.client.MongoCollection
import org.bson.Document

/**
 * Tenant gateway backed by Mongo database.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class MongoTenantGateway(tenants: MongoCollection<Document>): MongoGateway(tenants), TenantGateway {
    object NameField : StringField("name")
    private object DisplayNameField : StringField("displayName")
    private object CreatedAtField : LongField("createdAt")
    private object MetadataField : ObjectField("metadata")

    private val tenants get() = collection

    private fun <V : Any> getData(field: DocumentField<V>, value: V) =
            tenants.find(filter { field eq value })
                    .selectFields(NameField, IdField, DisplayNameField, CreatedAtField)
                    .limit(1)
                    .first()
                    .ifNull { EntityNotFoundException(value) }
                    .let {
                        TenantGateway.GetTenantDataQueryResult(
                                displayName = it extract DisplayNameField,
                                name = it extract NameField,
                                id = it extract IdField,
                                createdAt = it extract CreatedAtField)
                    }

    override fun get(name: String) = getData(NameField, name)

    private fun getData(id: MongoIdentifier) = getData(IdField, id.value)

    override fun get(id: Identifier) = getData(id as MongoIdentifier)

    private inline fun updateById(id: MongoIdentifier, update: UpdateBuilder.() -> Unit) {
        if (tenants.updateOne(filter { IdField eq id }, UpdateBuilder().apply(update).get()).matchedCount == 0L) {
            throw EntityNotFoundException(id)
        }
    }


    override fun updateName(id: Identifier, newName: String) = updateById(id as MongoIdentifier) {
        NameField set newName
    }


    override fun updateDisplayName(id: Identifier, displayName: String) = updateById(id as MongoIdentifier) {
        DisplayNameField set displayName
    }

    override fun create(name: String): MongoIdentifier =
            document {
                this[NameField] = name
                this[DisplayNameField] = ""
                this[CreatedAtField] = System.currentTimeMillis()
                this[MetadataField] = MongoDocument()
            }.let {
                try {
                    tenants.insertOne(it)
                } catch (e: DuplicateKeyException) {
                    throw DuplicateEntityException(name, e)
                }
                it extract IdField
            }

    private fun delete(id: MongoIdentifier) {
        tenants.deleteOne(filter { IdField eq id })
    }

    override fun delete(id: Identifier) = delete(id as MongoIdentifier)

    private fun updateMetadata(id: MongoIdentifier, namespace: String, metadata: Variant) =
            updateById(id, { MetadataField nested VariantField(namespace) set metadata })

    private fun updateMetadata(id: MongoIdentifier, namespace: String, metadata: Convertible) = updateMetadata(id, namespace, metadata.serialize())

    override fun updateMetadata(id: Identifier, namespace: String, metadata: Convertible) = updateMetadata(id as MongoIdentifier, namespace, metadata)

    private fun readMetadata(id: MongoIdentifier, namespace: String, metadata: Convertible): Boolean =
            (tenants.find(filter { IdField eq id })
                    .selectFields(MetadataField nested VariantField(namespace))
                    .limit(1)
                    .first()
                    .ifNull { EntityNotFoundException(id) } extract MetadataField)
                    .let {
                        try {
                            metadata.deserialize(it extract VariantField(namespace))
                        } catch (e: MissingFieldException) {
                            false
                        }
                    }

    override fun readMetadata(id: Identifier, namespace: String, metadata: Convertible) =
            readMetadata(id as MongoIdentifier, namespace, metadata)
}
MockPortal
====
MockPortal is a portal for QA engineers especially designed to Enteprise software based on SOAP or REST.

# Setup development environment

## MongoDB setup
This instruction is applicable for installation of MongoDB on VM.
1. `apt-get install mongodb` on you virtual machine. 
1. Configure network connection with Virtual Machine to make MongoDB available outside.
1. Go to `/etc/mongodb.conf`
1. Set `bind_ip=0.0.0`
1. Set `port=27017`
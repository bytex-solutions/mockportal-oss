package com.bytex.mockportal.web.api.users

import com.bytex.mockportal.User
import com.bytex.mockportal.UserRole
import com.bytex.mockportal.util.json.UnixTimeDeserializer
import com.bytex.mockportal.util.json.UnixTimeSerializer
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import java.time.Instant

/**
 * Represents user profile.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal data class UserProfile(@JsonProperty var login: String,
                                @JsonProperty var displayName: String,
                                @JsonProperty @JsonSerialize(using = UnixTimeSerializer::class) @JsonDeserialize(using = UnixTimeDeserializer::class) var lastLogin: Instant,
                                @JsonProperty @JsonSerialize(using = UnixTimeSerializer::class) @JsonDeserialize(using = UnixTimeDeserializer::class) var createdAt: Instant,
                                @JsonProperty var role: UserRole) {
    constructor(user: User) : this(user.login, user.displayName, user.lastLogin, user.createdAt, user.role)
}
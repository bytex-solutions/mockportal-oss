package com.bytex.mockportal

import com.bytex.mockportal.util.conversion.Convertible
import java.time.Instant

/**
 * Represents data about tenant.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
interface Tenant: Iterable<User> {
    /**
     * Metadata associated with the tenant.
     */
    interface Metadata: Convertible{
        /**
         * Namespace of metadata.
         */
        val namespace: String
    }

    /**
     * Gets or sets short name of tenant.
     * Acts like a domain name used in conjunction with user name.
     * @throws DuplicateTenantNameException Cannot rename tenant.
     */
    var name: String

    /**
     * Display name of tenant.
     */
    var displayName: String

    /**
     * Tenant creation time.
     */
    val createdAt: Instant

    /**
     * Deletes this tenant.
     * @param withUsers true to delete tenant with all users; false to delete tenant only at leave users exist.
     * @return Number of deleted or detached users.
     */
    fun delete(withUsers: Boolean): Long

    /**
     * Gets users in this tenant by role
     * @param role User role
     * @return Collection of users with the specified role.
     */
    operator fun get(role: UserRole): Iterable<User>

    /**
     * Associates user with this tenant.
     * @throws UserAssociationException User is already associated with another tenant.
     */
    fun linkUser(user: User)

    /**
     * Removes user from tenant
     * @param user User to be removed from tenant.
     * @return true, if user in this tenant; otherwise, false.
     */
    fun unlinkUser(user: User): Boolean

    /**
     * Writes metadata into this tenant.
     */
    fun Metadata.write()

    /**
     * Reads metadata associated with this tenant.
     */
    fun Metadata.read(): Boolean
}

operator fun Tenant.plusAssign(user: User) = linkUser(user)
operator fun Tenant.minusAssign(user: User){
    unlinkUser(user)
}
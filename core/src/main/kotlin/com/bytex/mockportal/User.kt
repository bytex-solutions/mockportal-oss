package com.bytex.mockportal

import com.bytex.mockportal.web.security.AuthenticationType
import org.springframework.security.oauth2.common.OAuth2RefreshToken
import java.time.Instant
import java.util.*

/**
 * Represents user in MockPortal.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
interface User {
    /**
     * Represents authentication type.
     */
    val accountType: AuthenticationType

    /**
     * Indicates that this user is enabled.
     */
    var enabled: Boolean

    /**
     * Get or sets user e-mail.
     */
    var email: String

    /**
     * Indicates token that identifies snapshot of security information associated with this user.
     */
    val passcode: UUID

    /**
     * Gets or sets user role.
     */
    var role: UserRole

    /**
     * Gets or sets OAuth2 access token associated with this user.
     */
    var refreshToken: OAuth2RefreshToken?

    /**
     * Gets login of this user.
     */
    val login: String

    /**
     * Gets or sets display name of user.
     */
    var displayName: String

    /**
     * Gets last login of the user in its timezone.
     */
    val lastLogin: Instant

    /**
     * Account creation time.
     */
    val createdAt:Instant

    /**
     * Reports that login is occured by user.
     */
    fun loginOccurs()

    /**
     * Reports that logout was initiated by user directly.
     */
    fun logoutOccurs()

    /**
     * Leaves tenant.
     * @return true, if user is detached from tenant; false, if user was not attached to tenant.
     */
    fun leave(): Boolean


    /**
     * Deletes this user from system.
     */
    fun delete()
}

infix fun User.join(t: Tenant){
    t += this
}

infix fun User.leave(t: Tenant){
    t -= this
}
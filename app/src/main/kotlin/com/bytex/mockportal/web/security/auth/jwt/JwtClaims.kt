package com.bytex.mockportal.web.security.auth.jwt

import com.bytex.mockportal.Permission
import com.bytex.mockportal.User
import com.bytex.mockportal.UserRole
import com.bytex.mockportal.util.json.UnixTimeDeserializer
import com.bytex.mockportal.util.json.UnixTimeSerializer
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.deserialize
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.stringify
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.toJSON
import com.bytex.mockportal.web.X_FORWARDED_HOST_REQUEST_HEADER
import com.bytex.mockportal.web.security.AuthenticationType
import com.bytex.mockportal.web.security.auth.Claim
import com.bytex.mockportal.web.security.auth.UserAuthentication
import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.UUIDDeserializer
import com.fasterxml.jackson.databind.ser.std.UUIDSerializer
import org.springframework.security.jwt.Jwt
import java.net.URL
import java.security.Principal
import java.time.Duration
import java.time.Instant
import java.util.*
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.collections.HashMap

internal val DEFAULT_TOKEN_TTL = Duration.ofHours(1L)

/**
 * Represents JWT claims
 * @param subject User name.
 * @param createdAt The time at which the JWT was issued.
 * @param passcode Unique identifier of user login session.
 */
internal data class JwtClaims(@JsonProperty("sub") var subject: String,
                             @JsonProperty("iat") @JsonSerialize(using = UnixTimeSerializer::class) @JsonDeserialize(using= UnixTimeDeserializer::class) var createdAt: Instant,
                             @JsonProperty("aud") @JsonSerialize(using = UUIDSerializer::class) @JsonDeserialize(using = UUIDDeserializer::class) override var passcode: UUID,
                             @JsonProperty("role") var role: UserRole,
                              @JsonProperty("eat") var authType: AuthenticationType): UserAuthentication.Internal(), Iterable<Claim> {
    private val details = HashMap<String, String>()

    constructor(account: User) : this(account.login, Instant.now(), account.passcode, account.role, account.accountType)

    fun importClaim(claim: Claim): Boolean = when (claim) {
        is Claim.Expiration -> {
            expiredAt = claim.expiredAt
            true
        }
        is Claim.Custom -> {
            details[claim.name] = claim.content
            true
        }
        else -> false
    }

    fun isRefreshRequired(renewalRate: Float, currentTime: Instant = Instant.now()): Boolean =
            Duration.between(currentTime, expiredAt).toMillis() < timeToLive.toMillis() * renewalRate

    fun refresh(claimsUpdater: ClaimsUpdater, ttl: Duration = DEFAULT_TOKEN_TTL) {
        createdAt = Instant.now()
        expiredAt = createdAt + ttl
        for (entry in details.entries) {
            val newContent = claimsUpdater(principal, entry.key, entry.value)
            if (newContent != entry.value)
                entry.setValue(newContent)
        }
    }

    @JsonAnyGetter
    @JsonAnySetter
    override fun getDetails(): Map<String, String> = details

    /**
     * Returns the name of this principal.
     *
     * @return the name of this principal.
     */
    @JsonIgnore
    override fun getName(): String = subject

    /**
     * Returns principal of this object.
     */
    @JsonIgnore
    override fun getPrincipal(): Principal = this

    /**
     * Indicates expiration time of token.
     */
    @JsonProperty("exp")
    @JsonSerialize(using = UnixTimeSerializer::class)
    @JsonDeserialize(using = UnixTimeDeserializer::class)
    var expiredAt: Instant = createdAt.plus(DEFAULT_TOKEN_TTL)

    /**
     *  Token time-to-live.
     */

    var timeToLive: Duration
        @JsonIgnore get() = Duration.between(expiredAt, createdAt)
        @JsonIgnore set(timeToLive) {
            expiredAt = createdAt + timeToLive
        }

    /**
     * Represents issuer of this token.
     */
    @JsonProperty("iss")
    var issuer: String = "mockportal"

    /**
     * Converts claims to JSON.
     */
    override fun toString(): String = toJSON().stringify()

    //jwt claims itself is a credentials of user
    @JsonIgnore
    override fun getCredentials(): JwtClaims = this

    @JsonIgnore
    override fun getAuthorities(): Set<Permission> = role.grantedAuthorities

    fun emitJwtAsCookie(request: HttpServletRequest, response: HttpServletResponse, jwts: JwtService, tokenCookie: String, restrictedHostName: String) {
        val hostName = URL(request.requestURL.toString()).host
        val jwt = jwts.createToken(toString())
        val cookie = Cookie(tokenCookie, jwt.encoded).apply {
            domain = if (restrictedHostName.isEmpty()) {
                val originalHost = request.getHeader(X_FORWARDED_HOST_REQUEST_HEADER)
                originalHost ?: hostName
            } else
                restrictedHostName
            secure = request.isSecure
            comment = "JWT token used by MockPortal"
            isHttpOnly = false
            maxAge = timeToLive.seconds.toInt()
        }
        response.addCookie(cookie)
    }

    /**
     * Returns an iterator over the elements of this object.
     */
    override fun iterator(): Iterator<Claim> {
        val wellKnownClaims = sequenceOf(Claim.Expiration(expiredAt), Claim.AuthType(authType))
        val customClaims = details.asSequence().map { Claim.Custom(it.key, it.value) }
        return (wellKnownClaims + customClaims).iterator()
    }
}

internal fun Jwt.createUserInternalAuthentication(): JwtClaims = JSON(claims) deserialize JwtClaims::class  ?: throw BadTokenException("Incorrect JWT Claims section")
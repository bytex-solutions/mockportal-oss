package com.bytex.mockportal.web.api

import com.bytex.mockportal.TenantAlreadyExistsException
import com.bytex.mockportal.TenantCreationException
import com.bytex.mockportal.UserAssociationException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import javax.servlet.http.HttpServletRequest

/**
 * Provides global error handling and other advices to REST API.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@ControllerAdvice(annotations = [RestController::class])
internal class ApiExceptionHandler: ResponseEntityExceptionHandler() {
    @ExceptionHandler(TenantCreationException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleException(request: HttpServletRequest, e: TenantCreationException): WebError {
        logger.error("Failed to create tenant ${e.name} by user ${request.remoteUser}. Request is ${request.requestURI}", e)
        return WebError(errorCode = "tenant_creation", message = e.message)
    }

    @ExceptionHandler(TenantAlreadyExistsException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleException(request: HttpServletRequest, e: TenantAlreadyExistsException): WebError {
        logger.error("Unable to assign tenant name ${e.name} by user ${request.remoteUser}. Request is ${request.requestURI}", e)
        return WebError(errorCode = "tenant_rename", message = "This tenant name is already occupied. Try another one")
    }

    @ExceptionHandler(UserAssociationException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleException(request: HttpServletRequest, e: UserAssociationException): WebError {
        logger.error("User ${e.login} is already associated with tenant. Request is ${request.requestURI}", e)
        return WebError(errorCode = "user_association", message = "You are already associated with the tenant")
    }
}
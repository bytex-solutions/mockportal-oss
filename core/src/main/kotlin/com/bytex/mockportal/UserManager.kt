package com.bytex.mockportal

import com.bytex.mockportal.serviceModel.ApplicationService
import com.bytex.mockportal.web.security.AuthenticationType
import java.util.*

/**
 * Represents a root interface for accessing users.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
interface UserManager: ApplicationService {
    /**
     * Creates a new user.
     * @throws UserAlreadyExistsException User with the specified login already exists.
     */
    fun create(login: String, role: UserRole, accountType: AuthenticationType): User

    /**
     * Gets user by its login.
     */
    operator fun get(login: String): User?

    /**
     * Indicates that passcode is in white list.
     * @param login User name.
     * @param passcode Passcode to check.
     */
    fun isValidPasscode(login: String, passcode: UUID): Boolean
}
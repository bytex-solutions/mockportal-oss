package com.bytex.mockportal.util

import kotlin.reflect.KClass
import kotlin.reflect.full.cast

inline fun <T> T?.ifNull(exception: () -> Exception): T = this ?: throw exception()

inline fun <T, R> T?.ifNotNull(exception: (T) -> Exception, action: () -> R): R = if(this == null) action() else throw exception(this)

inline fun <T: CharSequence> T.ifEmpty(action: () -> T): T = if(isEmpty()) action() else this

inline fun <T: Any> iterable(crossinline iteratorImpl: () -> Iterator<T>) = object : Iterable<T>{
    override fun iterator(): Iterator<T>  = iteratorImpl()
}

/**
 * Provides the same behavior as **as** keyword but for late-binding scenario.
 */
infix fun <T: Any> Any.into(type: KClass<T>): T = type.cast(this)

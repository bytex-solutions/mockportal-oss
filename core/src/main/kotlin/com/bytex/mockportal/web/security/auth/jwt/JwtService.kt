package com.bytex.mockportal.web.security.auth.jwt

import com.bytex.mockportal.serviceModel.ApplicationService
import org.springframework.security.jwt.Jwt

/**
 * Provides JWT encoding and decoding operations.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
interface JwtService: ApplicationService {
    fun decodeAndVerify(jwt: String): Jwt
    fun createToken(claims: Map<String, *>): Jwt
    fun createToken(claims: String): Jwt
}
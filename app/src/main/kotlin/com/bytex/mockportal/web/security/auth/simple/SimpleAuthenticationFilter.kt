package com.bytex.mockportal.web.security.auth.simple

import com.bytex.mockportal.User
import com.bytex.mockportal.web.security.AuthenticationType
import com.bytex.mockportal.web.security.auth.BasicExternalAuthenticationFilter
import com.bytex.mockportal.web.security.auth.UserAuthentication
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.userdetails.UsernameNotFoundException

/**
 *
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class SimpleAuthenticationFilter(loginPathPrefix: String, private val users: PredefinedUsers) : BasicExternalAuthenticationFilter(loginPathPrefix) {
    private class SimpleAuthentication(private val userName: String, private val account: PredefinedUserAccount) : UserAuthentication.External() {
        override fun updateUserProfile(user: User) {
            user.displayName = account.displayName
        }

        override fun getName() = userName

        override fun getCredentials() = account.password

        override fun getPrincipal() = this

        override fun getDetails() = account

        override val type: AuthenticationType = AuthenticationType.SIMPLE
    }

    override fun attemptAuthentication(token: BasicAuthToken): UserAuthentication.External {
        val account = users[token.name]
        return when {
            account == null -> throw UsernameNotFoundException("User ${token.name} doesn't exist")
            account.test(token.credentials) -> SimpleAuthentication(token.name, account)
            else -> throw BadCredentialsException("Incorrect password for user ${token.name}")
        }
    }
}
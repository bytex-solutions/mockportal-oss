package com.bytex.mockportal.dao.mongo

import org.bson.Document
import org.bson.conversions.Bson
import org.bson.types.ObjectId

/**
 * Represents MongoDB document bounded to filter DSL.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class FilterDocument: Document() {
    infix fun <V> DocumentField<V>.eq(value: V) {
        append(name, value)
    }

    infix fun DocumentField<ObjectId>.eq(value: MongoIdentifier) = eq(value.value)

    infix fun DocumentField<*>.exists(value: Boolean) {
        append(name, document { this["\$exists"] = value })
    }
}

/**
 * Starts MongoDB filter DSL.
 * @param MongoDB query statement.
 * @return Filter document.
 */
internal inline fun filter(body: FilterDocument.() -> Unit): Bson {
    val filter = FilterDocument()
    filter.body()
    return filter
}


package com.bytex.mockportal

/**
 * Indicates that tenant cannot be renamed because new name is already occupied.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
class DuplicateTenantNameException(name: String, cause: Throwable? = null): TenantException(name, "Tenant name $name is not unique", cause)
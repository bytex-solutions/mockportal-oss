package com.bytex.mockportal.web.security.auth.jwt

import com.bytex.mockportal.configuration.JWT_SECRET_PROPERTY
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.stringify
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.toJSON
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.jwt.Jwt
import org.springframework.security.jwt.JwtHelper
import org.springframework.security.jwt.crypto.sign.MacSigner
import org.springframework.stereotype.Service

/**
 * Represents default implementation of JWT encoder/decoder.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@Service
class DefaultJwtService(@Value("\${$JWT_SECRET_PROPERTY}") tokenSecret: String): MacSigner(tokenSecret), JwtService {
    override fun createToken(claims: String): Jwt = JwtHelper.encode(claims, this)

    override fun decodeAndVerify(jwt: String): Jwt = JwtHelper.decodeAndVerify(jwt, this)

    override fun createToken(claims: Map<String, *>): Jwt = createToken(claims.toJSON().stringify())
}
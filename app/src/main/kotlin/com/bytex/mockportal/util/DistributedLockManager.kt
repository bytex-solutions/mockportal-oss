package com.bytex.mockportal.util

import com.hazelcast.core.HazelcastInstance
import org.springframework.stereotype.Service
import java.util.concurrent.locks.Lock

/**
 * Represents distributed lock manager backed by
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@Service(LOCK_MANAGER_SERVICE)
internal class DistributedLockManager(private val hazelcast: HazelcastInstance): LockManager {
    /**
     * Gets lock by its name.
     */
    override fun get(lockName: String): Lock = hazelcast.getLock(lockName)
}
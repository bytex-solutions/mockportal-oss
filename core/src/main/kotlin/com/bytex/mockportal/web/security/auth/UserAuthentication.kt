package com.bytex.mockportal.web.security.auth

import com.bytex.mockportal.Permission
import com.bytex.mockportal.User
import com.bytex.mockportal.web.security.AuthenticationType
import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.core.Authentication
import java.net.InetSocketAddress
import java.security.Principal
import java.util.*

/**
 * Represents information about user authentication in MockPortal.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
sealed class UserAuthentication(private var authenticated: Boolean): Authentication {
    @JsonIgnore
    final override fun setAuthenticated(value: Boolean) {
        authenticated = value
    }

    @JsonIgnore
    final override fun isAuthenticated(): Boolean = authenticated

    /**
     * Gets user permissions.
     */
    abstract override fun getAuthorities(): Set<Permission>

    /**
     * Represents successful user authentication.
     */
    abstract class Successful private constructor(val source: UserAuthentication): UserAuthentication(true) {
        /**
         * Wraps external authentication into successful authentication.
         * @param source Successfully checked external authentication.
         */
        protected constructor(source: External) : this(source as UserAuthentication)

        /**
         * Wraps internal authentication into successfully authentication.
         * @param source Successfully checked internal authentication.
         */
        protected constructor(source: Internal) : this(source as UserAuthentication)

        abstract override fun getPrincipal(): User

        override fun getAuthorities(): Set<Permission> = principal.role.grantedAuthorities

        override fun getName(): String = principal.login

        override fun getCredentials(): Any = source.credentials

        override fun getDetails(): Any = source.details

        inline fun <reified T : UserAuthentication> applyToSourceIf(checker: T.() -> Boolean = { true }, applier: T.() -> Unit): Boolean =
                if (source is T && checker(source)) {
                    applier(source)
                    true
                } else
                    false
    }

    /**
     * Represents result of authentication in external service such as Google OAuth2, LDAP etc.
     */
    abstract class External(authenticated: Boolean = false): UserAuthentication(authenticated) {
        /**
         * Export additional claims.
         * @param acceptor Claim acceptor. It returns true, if claim is supported and can be accepted.
         */
        open fun exportClaims(acceptor: (Claim) -> Boolean){
            
        }

        /**
         * Gets type of authentication.
         */
        abstract val type: AuthenticationType

        /**
         * Updates user profile according with information supplied from external authentication provider.
         *
         * Implementation of this method can override role and permissions if they were supplied
         * by external authentication mechanism such as corporate directory services or SSO.
         *
         * @param user User profile to be updated.
         */
        abstract fun updateUserProfile(user: User)

        /**
         * By default, external authentication doesn't provide authorities.
         */
        override fun getAuthorities(): Set<Permission> = emptySet()
    }

    /**
     * Represents result of authentication in MockPortal.
     * @author Roman Sakno
     * @since 1.0
     * @version 1.0
     */
    abstract class Internal(authenticated: Boolean = true): UserAuthentication(authenticated), Iterable<Claim> {
        /**
         * Gets passcode.
         */
        abstract val passcode: UUID

        override fun getPrincipal(): Principal = this
    }

    /**
     * Represents anonymous user.
     */
    abstract class Anonymous: UserAuthentication(false){
        /**
         * Anonymous user doesn't have any permissions.
         */
        final override fun getAuthorities(): Set<Permission> = emptySet()

        final override fun getPrincipal(): Principal = this

        abstract override fun getCredentials(): InetSocketAddress

        override fun getName(): String = "anonymous"
    }
}
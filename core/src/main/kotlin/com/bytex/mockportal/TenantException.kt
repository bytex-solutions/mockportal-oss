package com.bytex.mockportal

/**
 * Indicates that something wrong with tenant.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
abstract class TenantException(val name: String, message: String, cause: Throwable? = null): MockPortalException(message, cause)
package com.bytex.mockportal.util.conversion

import com.bytex.mockportal.web.api.WebError
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.TextNode
import com.fasterxml.jackson.databind.node.TreeTraversingParser
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.util.MimeType
import org.springframework.util.MimeTypeUtils.*
import java.util.*
import kotlin.reflect.KClass

/**
 * Represents serialization format.
 * @param F Underlying JVM type.
 * @since 1.0
 * @version 1.0
 */
sealed class ConversionFormat<F: Any>(val mimeType: MimeType, val nativeType: KClass<F>){
    final override fun toString(): String = mimeType.toString()
    final override fun equals(other: Any?): Boolean = other is ConversionFormat<*> &&
            mimeType == other.mimeType &&
            nativeType == other.nativeType
    final override fun hashCode(): Int = Objects.hash(mimeType, nativeType)

    /**
     * JSON format.
     */
    object JSON: ConversionFormat<JsonNode>(APPLICATION_JSON, JsonNode::class) {
        private val mapper = ObjectMapper().apply { registerKotlinModule() }

        fun Any.toJSON(): JsonNode = mapper.valueToTree(this)
        fun String.toJSON(): TextNode = TextNode.valueOf(this)
        fun WebError.toJSON(): ObjectNode = mapper.valueToTree(this)
        fun JsonNode.stringify(): String = mapper.writeValueAsString(this)
        operator fun invoke(input: String): JsonNode = mapper.readTree(input)
        fun createConverter() = MappingJackson2HttpMessageConverter(mapper)
        infix fun <T : Any> JsonNode.deserialize(type: KClass<T>): T? = mapper.treeToValue(this, type.java)
        infix fun <T : Any> JsonNode.deserialize(instance: T) = TreeTraversingParser(this).use<JsonParser, Unit> { mapper.readerForUpdating(instance).readValue<T>(it) }
    }

    /**
     * Plain text format.
     */
    object PlainText:  ConversionFormat<String>(TEXT_PLAIN, String::class)

    /**
     * Binary format.
     */
    object Binary: ConversionFormat<ByteArray>(APPLICATION_OCTET_STREAM, ByteArray::class)
}
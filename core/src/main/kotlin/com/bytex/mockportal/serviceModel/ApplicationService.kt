package com.bytex.mockportal.serviceModel

/**
 * Represents application service that can be injected by its name and interface.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
interface ApplicationService {
    /**
     * Query aggregated service.
     * @param serviceType Type of requested service.
     * @return Service instance or null, if service is not supported.
     */
    operator fun <S> invoke(serviceType: Class<S>): S? = if (serviceType.isInstance(this)) serviceType.cast(this) else null
}
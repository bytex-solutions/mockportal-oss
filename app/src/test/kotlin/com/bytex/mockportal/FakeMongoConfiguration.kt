package com.bytex.mockportal

import com.bytex.mockportal.configuration.MONGO_DATABASE_NAME_PROPERTY
import com.bytex.mockportal.dao.mongo.*
import com.github.fakemongo.Fongo
import com.mongodb.MongoClient
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.IndexOptions
import com.mongodb.connection.ServerVersion
import org.bson.Document
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.config.AbstractMongoConfiguration
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

/**
 *
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@Configuration
@EnableAutoConfiguration(exclude = [MongoAutoConfiguration::class])
internal open class FakeMongoConfiguration(@Value("\${$MONGO_DATABASE_NAME_PROPERTY}") private val databaseName: String): AbstractMongoConfiguration() {
    private val fongo = Fongo("TestDatabase", ServerVersion(listOf(3, 2, 11)))

    private class IndexDefinition: Document(){
        fun DocumentField<*>.asc(){
            append(name, 1)
        }

        fun DocumentField<*>.desc(){
            append(name, -1)
        }
    }

    private fun MongoCollection<*>.createIndex(defineOptions: IndexOptions.() -> Unit = {}, indexer: IndexDefinition.() -> Unit) {
        val options = IndexOptions()
        options.defineOptions()
        val index = IndexDefinition()
        index.indexer()
        createIndex(index, options)
    }

    private fun uniqueIndex(): IndexOptions = IndexOptions().unique(true)

    @PostConstruct
    fun setupDabase() {
        val database = fongo.getDatabase(databaseName)
        var collection: MongoCollection<Document>
        //setup users
        database.createCollection(USERS_COLLECTION)
        collection = database.getCollection(USERS_COLLECTION)
        collection.createIndex({ unique(true) }) { MongoUserGateway.LoginField.asc() }
        collection.createIndex { MongoUserGateway.TenantField.asc() }
        //setup tenants
        database.createCollection(TENANTS_COLLECTION)
        collection = database.getCollection(TENANTS_COLLECTION)
        collection.createIndex({ unique(true) }) { MongoTenantGateway.NameField.asc() }
    }

    @Bean("mongo")
    override fun mongoClient(): MongoClient = fongo.mongo

    override fun getDatabaseName(): String = databaseName

    @PreDestroy
    fun destroy(){
        fongo.mongo.close()    
    }
}
package com.bytex.mockportal.web.api

import com.bytex.mockportal.User
import com.bytex.mockportal.util.conversion.ConversionFormat
import com.bytex.mockportal.web.security.auth.UserAuthentication
import org.springframework.context.annotation.Configuration
import org.springframework.core.MethodParameter
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.web.bind.support.WebDataBinderFactory
import org.springframework.web.context.request.NativeWebRequest
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.method.support.ModelAndViewContainer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

/**
 * Provides configuration of REST API.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@Configuration
open class ApiConfiguration: WebMvcConfigurer {
    private object UserArgumentResolver : HandlerMethodArgumentResolver {
        override fun supportsParameter(parameter: MethodParameter) =
                parameter.parameterType == User::class.java

        override fun resolveArgument(parameter: MethodParameter, container: ModelAndViewContainer?, request: NativeWebRequest, binderFactory: WebDataBinderFactory?): User? =
                (request.userPrincipal as? UserAuthentication.Successful)?.principal
    }

    final override fun addArgumentResolvers(argumentResolvers: MutableList<HandlerMethodArgumentResolver>) {
        argumentResolvers.add(UserArgumentResolver)
    }

    override fun extendMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        converters.add(ConversionFormat.JSON.createConverter())
    }
}
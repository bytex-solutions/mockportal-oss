package com.bytex.mockportal

import org.springframework.security.core.authority.GrantedAuthoritiesContainer
import java.net.URI
import java.util.*

/**
 * Represents roles available in MockPortal.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
enum class UserRole(roleName: String, first: Permission, vararg permissions: Permission): GrantedAuthoritiesContainer {
    TENANT_ADMIN("tenant-admin", Permission.EXECUTE_MOCK_REQUESTS, *Permission.values()),
    QA_ENGINEER("qa-engineer", Permission.EXECUTE_MOCK_REQUESTS);

    companion object {
        fun valueOf(value: URI) =
                values().find { it.urn == value }
                        ?: throw IllegalArgumentException("Role URN $value cannot be mapped correctly")

    }

    private val authorities = EnumSet.of<Permission>(first, *permissions)

    val urn = URI("urn:com:bytex:mockportal:security:role:$roleName")

    /**
     * Gets permissions associated with this role.
     */
    override fun getGrantedAuthorities(): Set<Permission> = authorities

    override fun toString(): String = urn.toString()
}
package com.bytex.mockportal.web.api.users

import com.bytex.mockportal.User
import com.bytex.mockportal.web.WEB_API_PATH_PREFIX
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

/**
 * Provides information about user through REST service.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@RequestMapping("$WEB_API_PATH_PREFIX/logout")
@RestController
open class LogoutService {

    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("isAuthenticated()")
    open fun logout(user: User) = user.logoutOccurs()
}
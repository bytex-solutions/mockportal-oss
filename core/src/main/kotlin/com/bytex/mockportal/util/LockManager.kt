package com.bytex.mockportal.util

import com.bytex.mockportal.serviceModel.ApplicationService
import java.util.concurrent.locks.Lock

const val LOCK_MANAGER_SERVICE = "lockManager"

/**
 * Represents lock manager.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
interface LockManager: ApplicationService {
    /**
     * Gets lock by its name.
     */
    fun get(lockName: String): Lock
}
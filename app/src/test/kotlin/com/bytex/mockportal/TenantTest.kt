package com.bytex.mockportal

import com.bytex.mockportal.configuration.Profiles
import com.bytex.mockportal.util.conversion.ConversionFormat
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.deserialize
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.toJSON
import com.bytex.mockportal.util.into
import com.bytex.mockportal.web.security.AuthenticationType
import com.fasterxml.jackson.databind.JsonNode
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.serializer.support.SerializationFailedException
import org.springframework.test.context.ActiveProfiles
import java.util.*
import kotlin.test.*

/**
 *
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@ActiveProfiles(Profiles.ON_PREMISE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
internal class TenantTest: IntegrationTest() {
    @Autowired
    private lateinit var users: UserManager
    @Autowired
    private lateinit var tenants: TenantManager

    @Test
    fun createDeleteTenant() {
        val user = users.create("john", UserRole.TENANT_ADMIN, AuthenticationType.SIMPLE)
        val tenant = tenants.create("Acme", user)
        assertNotNull(tenants["Acme"])
        assertEquals(tenant, with(tenants) { user.tenant })
        //attempt to create second tenant with the same owner
        assertNull(try {
            tenants.create("Evil Corporation", user)
        } catch (e: UserAssociationException) {
            null
        })
        //detach user from tenant
        assertTrue(user.leave())
        assertNull(with(tenants) { user.tenant })
        //attempt to create tenant with the same name
        assertNull(try{
            tenants.create("Acme", user)
        } catch (e: TenantAlreadyExistsException){
            null
        })
        //delete tenant
        assertEquals(0, tenant.delete(false))
        assertNull(tenants["Acme"])
        user.delete()
    }

    @Test
    fun membershipTest(){
        val user1 = users.create("john", UserRole.TENANT_ADMIN, AuthenticationType.SIMPLE)
        val user2 = users.create("barry", UserRole.QA_ENGINEER, AuthenticationType.SIMPLE)
        val tenant = tenants.create("Acme", user1)
        assertNotNull(this.tenants["Acme"])
        //the same user can be linked twice. This operation has no effect
        user1 join tenant
        //link another user
        user2 join tenant
        //iterate over all users
        val users = LinkedList<User>()
        tenant.toCollection(users)
        assertEquals(2, users.size)
        assertTrue(users.contains(user1))
        assertTrue(users.contains(user2))
        //remove 1 user and iterate again
        user1.leave()
        users.clear()
        tenant.toCollection(users)
        assertEquals(1, users.size)
        assertFalse(users.contains(user1))
        assertTrue(users.contains(user2))
        //link first user again
        user1 join tenant
        //select users by their roles
        users.clear()
        tenant[UserRole.QA_ENGINEER].toCollection(users)
        assertEquals(1, users.size)
        assertTrue(users.contains(user2))
        users.clear()
        tenant[UserRole.TENANT_ADMIN].toCollection(users)
        assertEquals(1, users.size)
        assertTrue(users.contains(user1))
        //delete tenant without users
        tenant.delete(true)
        assertNull(this.users["john"])
        assertNull(this.users["barry"])
        assertNull(this.tenants["Acme"])
    }

    private data class TenantJsonMetadata(var strField: String, var boolField: Boolean, val map: MutableMap<String, String>): Tenant.Metadata {
        override val namespace: String = "TestMetadata"

        override fun supportsConversion(format: ConversionFormat<*>): Boolean = format === ConversionFormat.JSON

        override fun <F : Any> convertTo(format: ConversionFormat<F>): F = when (format) {
            ConversionFormat.JSON -> toJSON() into format.nativeType
            else -> throw SerializationFailedException("Format $format is not supported")
        }

        override fun <F : Any> convertFrom(format: ConversionFormat<F>, input: F) = when (format) {
            ConversionFormat.JSON -> input as JsonNode deserialize this
            else -> throw SerializationFailedException("Format $format is not supported")
        }
    }

    @Test
    fun metadataTest(){
        val user1 = users.create("john", UserRole.TENANT_ADMIN, AuthenticationType.SIMPLE)
        val tenant = tenants.create("Acme", user1)
        val meta = TenantJsonMetadata("string val", true, mutableMapOf("a" to "b", "c" to "d"))
        with(tenant){
            meta.write()
        }
        with(meta){
            strField = ""
            boolField = false
            map.clear()
        }
        with(tenant){
            meta.read()
        }
        assertEquals("string val", meta.strField)
        assertEquals(true, meta.boolField)
        assertEquals(mutableMapOf("a" to "b", "c" to "d"), meta.map)
    }
}
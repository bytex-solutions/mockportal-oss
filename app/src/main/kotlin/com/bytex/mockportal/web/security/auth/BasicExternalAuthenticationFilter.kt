package com.bytex.mockportal.web.security.auth

import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationServiceException
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import java.util.*
import java.util.regex.Pattern
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Represents basic external authentication filter
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal abstract class BasicExternalAuthenticationFilter(loginPathPrefix: String) : ExternalAuthenticationFilter("basicAuthFilter", AntPathRequestMatcher(loginPathPrefix, HttpMethod.GET.name)) {
    internal data class BasicAuthToken(private val userName: String, private var password: String): UsernamePasswordAuthenticationToken(userName, password) {
        override fun getName(): String = userName
        
        override fun getCredentials(): String = password

        override fun eraseCredentials() {
            password = ""
        }
    }

    private companion object TokenCodec {
        private val ENCODING = Charsets.UTF_8
        private val SPLITTER = Pattern.compile(":", Pattern.LITERAL)
        private const val VALUE_PREFIX = "Basic "

        internal fun tryDecode(token: String): BasicAuthToken? {
            if (token.startsWith(VALUE_PREFIX)) {
                @Suppress("NAME_SHADOWING")
                var token = token.substringAfter(VALUE_PREFIX)
                val decoded = try {
                    Base64.getDecoder().decode(token)
                } catch (e: IllegalArgumentException) {
                    return null
                }
                token = String(decoded, ENCODING)
                return token.split(SPLITTER, 2).let { BasicAuthToken(it[0], it[1]) }
            } else
                return null
        }
    }

    /**
     * Provides authentication using user name and password
     * @param token Decoded authentication token.
     * @return Result of authentication
     * @throws AuthenticationException Incorrect credentials.
     */
    protected abstract fun attemptAuthentication(token: BasicAuthToken): UserAuthentication.External

    @Value("\${spring.application.name}")
    var realm: String = "MP"

    private inline fun HttpServletResponse.unauthorized(exception: () -> AuthenticationException): Nothing {
        addHeader(HttpHeaders.WWW_AUTHENTICATE, "Basic realm=\"$realm\"")
        throw exception()
    }

    private fun attemptAuthentication(authorizationHeader: String, response: HttpServletResponse): UserAuthentication.External? =
            tryDecode(authorizationHeader)
                    ?.let(this::attemptAuthentication)
                    ?: response.unauthorized { throw BadCredentialsException("Failed to decode basic authentication token") }

    final override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): UserAuthentication.External? {
        return request.getHeader(HttpHeaders.AUTHORIZATION)?.let { attemptAuthentication(it, response) }
                ?: response.unauthorized { AuthenticationServiceException("Basic authentication required") }
    }
}
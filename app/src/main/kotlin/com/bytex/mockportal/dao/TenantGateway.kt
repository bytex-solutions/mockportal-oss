package com.bytex.mockportal.dao

import com.bytex.mockportal.serviceModel.ApplicationService
import com.bytex.mockportal.util.conversion.Convertible

/**
 * Represents low-level access to tenant data.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal interface TenantGateway: ApplicationService {
    data class GetTenantDataQueryResult(val displayName: String, val id: Identifier, val name: String, val createdAt: Long)

    /**
     * Gets tenant data by its name.
     * @param name Name of tenant.
     * @throws EntityNotFoundException Unable to find tenant by its name.
     */
    operator fun get(name: String): GetTenantDataQueryResult

    /**
     * Gets tenant data by its identifier.
     * @param id Identifier.
     * @throws EntityNotFoundException Unable to find tenant by its identifier.
     */
    operator fun get(id: Identifier): GetTenantDataQueryResult

    /**
     * Updates name of tenant.
     * @param id Identifier of the tenant.
     * @param newName New name of tenant.
     * @throws EntityNotFoundException Tenant with old name doesn't exist.
     * @throws DuplicateEntityException Tenant with new name already exist.
     */
    fun updateName(id: Identifier, newName: String)

    /**
     * Updates display name of tenant.
     * @param id Identifier of the tenant.
     * @param displayName New display name to be assigned.
     * @throws EntityNotFoundException Tenant with the specified identifier doesn't exist.
     */
    fun updateDisplayName(id: Identifier, displayName: String)

    /**
     * Creates a new tenant with short name.
     * @return Identifier of created tenant.
     * @throws DuplicateEntityException Tenant with specified name already exist.
     */
    fun create(name: String): Identifier

    /**
     * Deletes tenant
     */
    fun delete(id: Identifier)

    /**
     * Updates metadata associated with the tenant.
     * @param id Tenant identifier.
     * @param namespace Metadata namespace.
     * @param metadata Metadata to write.
     * @throws EntityNotFoundException Tenant with the specified identifier doesn't exist.
     */
    fun updateMetadata(id: Identifier, namespace: String, metadata: Convertible)

    /**
     * Restores metadata associated with the specified tenant identifier.
     * @param id Tenant identifier.
     * @param namespace Metadata namespace.
     * @param metadata Metadata to restore.
     * @return true, if metadata is associated with the specified namespace; otherwise, false.
     * @throws EntityNotFoundException Tenant with the specified identifier doesn't exist.
     */
    fun readMetadata(id: Identifier, namespace: String, metadata: Convertible): Boolean
}
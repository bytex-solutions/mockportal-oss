package com.bytex.mockportal

import com.bytex.mockportal.serviceModel.ApplicationService

/**
 * Represents a root interface for accessing tenants.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
interface TenantManager: ApplicationService {
    /**
     * Gets tenant by its name.
     */
    operator fun get(name: String): Tenant?

    /**
     * Gets tenant of the user.
     */
    val User.tenant: Tenant?

    /**
     * Creates a new tenant and assign the specified user as tenant owner.
     * @param name Short name of tenant
     * @param owner Owner of tenant.
     * @return Created tenant.
     * @throws TenantCreationException Specified user already a member of tenant.
     * @throws TenantAlreadyExistsException Tenant with this name already exist.
     * @throws UserAssociationException The specified user is already associated with the tenant.
     */
    fun create(name: String, owner: User): Tenant
}
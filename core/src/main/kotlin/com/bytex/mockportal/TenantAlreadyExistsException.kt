package com.bytex.mockportal

/**
 * Indicates that tenant with the specified name already exists.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
class TenantAlreadyExistsException(name: String, cause: Throwable? = null) : TenantException(name, "Tenant with $name already exists", cause)
package com.bytex.mockportal.util

import org.springframework.context.annotation.Condition

operator fun Condition.not() = Condition { context, metadata -> !matches(context, metadata)}
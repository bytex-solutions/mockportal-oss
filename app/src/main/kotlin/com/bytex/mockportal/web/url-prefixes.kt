package com.bytex.mockportal.web

/**
 * Prefix of login page.
 */
internal const val LOGIN_PAGE = "/login"

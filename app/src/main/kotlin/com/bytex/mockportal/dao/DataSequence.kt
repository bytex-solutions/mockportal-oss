package com.bytex.mockportal.dao

import org.springframework.data.util.CloseableIterator

/**
 * Represents data sequence with closeable iterator.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
interface DataSequence<T>: Iterable<T>, Sequence<T> {
    /**
     * Obtains closeable cursor over data.
     */
    override fun iterator(): CloseableIterator<T>
}
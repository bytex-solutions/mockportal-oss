package com.bytex.mockportal.dao

import com.bytex.mockportal.serviceModel.ApplicationService
import java.util.*

/**
 * Provides low-level access to user data.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal interface UserGateway: ApplicationService {
    data class GetUserDataQueryResult(val login: String,
                                      val authenticationType: String,
                                      val refreshToken: String,
                                      val lastLogin: Long,
                                      val createdAt: Long,
                                      val displayName: String,
                                      val enabled: Boolean,
                                      val passcode: UUID,
                                      val role: String,
                                      val email: String)

    /**
     * Creates a new user in database.
     * @throws DuplicateEntityException User with specified login already exists in database.
     */
    fun create(login: String, role: String, accountType: String)

    /**
     * Updates OAuth2 refresh token for the specified user.
     * @param login User login.
     * @param refreshToken OAuth2 refresh token in serializable format.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    fun updateRefreshToken(login: String, refreshToken: String)

    /**
     * Updates status of the user account.
     * @param login User login.
     * @param enabled Account status.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    fun updateUserStatus(login: String, enabled: Boolean)

    /**
     * Updates display name of the specified user.
     * @param login User login.
     * @param displayName Display name of the user, e.g. first name and last name.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    fun updateDisplayName(login: String, displayName: String)

    /**
     * Updates user role.
     * @param login User login.
     * @param role New user role.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    fun updateRole(login: String, role: String)

    /**
     * Updates last login time for the specified user.
     * @param login User login.
     * @param timeStamp Unix time stamp.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    fun updateLoginTime(login: String, timeStamp: Long)

    /**
     * Retrieves user data by its login.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    operator fun get(login: String): GetUserDataQueryResult

    /**
     * Gets tenant identifier associated with user.
     * @throws EntityNotFoundException
     */
    fun findTenant(login: String): Identifier?

    /**
     * Assigns tenant to the user.
     * @param login Login of the user.
     * @param tenantId Tenant identifier.
     * @return true, if user is assigned to tenant; false, if user already assigned to tenant.
     */
    fun updateTenant(login: String, tenantId: Identifier): Boolean

    /**
     * Detaches all users from the specified tenant.
     * @param tenantId Identifier of tenant.
     * @return Number of detached users.
     */
    fun detachFromTenant(tenantId: Identifier): Long

    /**
     * Detaches the specified user from tenant.
     * @param login User login.
     * @param tenantId Identifier of tenant. If null then detaches from any tenant.
     * @return true, if user is detached successfully; false, if user was not attached to tenant.
     */
    fun detachFromTenant(login: String, tenantId: Identifier? = null): Boolean

    /**
     * Gets iterator over all users associated with the tenant.
     * @param tenantId Identifier of tenant.
     * @return Iterator over user log-ins.
     */
    fun findAttachedUsers(tenantId: Identifier, role: String = ""): DataSequence<String>

    /**
     * Deletes all users associated with the specified tenant.
     * @param tenantId Identifier of tenant.
     * @return Number of deleted users.
     */
    fun deleteByTenant(tenantId: Identifier): Long

    /**
     * Deletes user from the database.
     * @param login Login of the user to delete.
     * @return true, if user is deleted.
     */
    fun delete(login: String): Boolean

    /**
     * Generates a new passcode and associate it with the specified user.
     * @param login User login.
     * @throws EntityNotFoundException User with the specified login doesn't exist.
     */
    fun updatePasscode(login: String): UUID

    /**
     * Checks whether the specified passcode is equal to passcode associated with the user.
     * @param login User login.
     * @param expected Passcode to check.
     * @return true, if passcode is correct
     */
    fun checkPasscode(login: String, expected: UUID): Boolean

    /**
     * Updates account e-mail.
     * @param login User login.
     * @param email User e-mail to set.
     * @throws EntityNotFoundException User with the specified login doesn't exist.
     */
    fun updateEmail(login: String, email: String)
}
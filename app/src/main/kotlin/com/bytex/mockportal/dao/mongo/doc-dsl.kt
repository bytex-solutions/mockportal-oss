package com.bytex.mockportal.dao.mongo

import com.bytex.mockportal.util.toByteArray
import org.bson.Document
import org.bson.types.Binary
import org.bson.types.ObjectId
import java.util.*


internal infix fun Document.extract(field: BinaryField) = field.readValue(this).data
internal infix fun <V> Document.extract(field: DocumentField<V>): V = field.readValue(this)
internal infix fun Document.extract(field: ObjectIdField) = MongoIdentifier(field.readValue(this))

/**
 * Represents MongoDB document bounded to BSON DSL.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class MongoDocument: Document() {
    operator fun <V> set(field: DocumentField<V>, value: V) {
        append(field.name, value)
    }

    operator fun set(field: DocumentField<Binary>, value: ByteArray) = set(field, Binary(value))
    operator fun set(field: DocumentField<Binary>, value: UUID) = set(field, Binary(value.toByteArray()))
    operator fun set(field: DocumentField<ObjectId>, value: MongoIdentifier) = set(field, value.value)
}

/**
 * Starts document description.
 * @param body Modifier of document.
 * @return MongoDB document.
 */
internal inline fun document(body: MongoDocument.() -> Unit): Document {
    val bson = MongoDocument()
    bson.body()
    return bson
}


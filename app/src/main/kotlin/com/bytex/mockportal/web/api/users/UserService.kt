package com.bytex.mockportal.web.api.users

import com.bytex.mockportal.Permission
import com.bytex.mockportal.TenantManager
import com.bytex.mockportal.User
import com.bytex.mockportal.web.WEB_API_PATH_PREFIX
import com.bytex.mockportal.web.security.auth.access.Authorization
import com.fasterxml.jackson.databind.node.TextNode
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.toJSON

/**
 * Common REST API to work with user account and tenant.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@Controller
@RequestMapping("$WEB_API_PATH_PREFIX/user")
internal open class UserService(@Autowired private val tenants: TenantManager) {

    /**
     * Gets user profile.
     */
    @GetMapping("/", produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody
    @PreAuthorize("isAuthenticated()")
    @ResponseStatus(HttpStatus.OK)
    open fun profile(user: User) = UserProfile(user)

    /**
     * Gets user e-mail.
     */
    @GetMapping("/email", produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody
    @PreAuthorize("isAuthenticated()")
    @ResponseStatus(HttpStatus.OK)
    open fun email(user: User) = user.email.toJSON()

    /**
     * Modifies user e-mail.
     */
    @PutMapping("/email", consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize("isAuthenticated()")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    open fun email(@RequestBody value: TextNode, user: User) {
        user.email = value.textValue()
    }

    @GetMapping("/displayName", produces = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize("isAuthenticated()")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    open fun displayName(user: User): TextNode = user.displayName.toJSON()

    @PutMapping("/displayName", consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize("isAuthenticated()")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    open fun displayName(@RequestBody value: TextNode, user: User) {
        user.displayName = value.textValue()
    }

    /**
     * Creates a new tenant if it is not associated with the user.
     */
    @PostMapping("/tenant", consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Authorization(Permission.TENANT_MANAGEMENT)
    open fun createTenant(@RequestBody tenant: TenantProfile, user: User) {
        with(tenants.create(tenant.name, user)) {
            displayName = tenant.displayName
        }
    }
}
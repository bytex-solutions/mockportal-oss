package com.bytex.mockportal.util

/**
 * Represents mapped lazy value.
 */
interface MappedLazy<out T>: Lazy<T>{
    /**
     * Gets underlying lazy value.
     */
    val underlying: Lazy<*>
}

inline fun <I, O> Lazy<I>.map(crossinline mapper: (I) -> O): Lazy<O> = object: MappedLazy<O> {
    override val value: O
        get() = mapper(this@map.value)

    override fun isInitialized(): Boolean = this@map.isInitialized()

    override val underlying: Lazy<*>
        get() = this@map

    override fun hashCode(): Int = this@map.hashCode()

    override fun equals(other: Any?): Boolean = javaClass.isInstance(other) &&
            other is MappedLazy<*> &&
            underlying == other.underlying
}

inline fun <reified T: Any> Lazy<*>.cast() = map { it as T }
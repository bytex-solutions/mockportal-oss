package com.bytex.mockportal.dao.mongo

import com.bytex.mockportal.dao.DataSequence
import com.bytex.mockportal.serviceModel.ApplicationService
import com.mongodb.client.FindIterable
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoCursor
import com.mongodb.client.MongoIterable
import com.mongodb.client.model.Projections
import org.bson.Document
import org.springframework.data.util.CloseableIterator
import java.util.function.Consumer

/**
 * Abstract class for data access gateway backed by Mongo.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal abstract class MongoGateway(protected val collection: MongoCollection<Document>): ApplicationService {
    /**
     * Bridge between MongoDB cursor and closeable iterator.
     */
    private class MongoCursorWrapper<T>(private val cursor: MongoCursor<T>) : CloseableIterator<T> {
        override fun hasNext(): Boolean = cursor.hasNext()

        override fun next(): T = cursor.next()

        override fun remove() = cursor.remove()

        override fun close() = cursor.close()

        override fun hashCode(): Int = cursor.hashCode()
        override fun equals(other: Any?): Boolean = other is MongoCursorWrapper<*> && cursor == other.cursor
        override fun forEachRemaining(action: Consumer<in T>) = cursor.forEachRemaining(action)
        override fun toString(): String = cursor.toString()
    }

    /**
     * Bridge between MongoDB iterable sequence and DAO sequence.
     */
    private class MongoDataSequence<T>(private val iterable: MongoIterable<T>) : DataSequence<T> {
        override fun iterator(): CloseableIterator<T> = MongoCursorWrapper(iterable.iterator())
        override fun equals(other: Any?): Boolean = other is MongoDataSequence<*> && iterable == other.iterable
        override fun hashCode(): Int = iterable.hashCode()
        override fun toString(): String = iterable.toString()
    }

    protected fun <T> FindIterable<T>.selectFields(vararg fields: DocumentField<*>) = projection(Projections.include(fields.map { it.name }))!!

    protected fun <T> MongoIterable<T>.wrap(): DataSequence<T> = MongoDataSequence(this)
}
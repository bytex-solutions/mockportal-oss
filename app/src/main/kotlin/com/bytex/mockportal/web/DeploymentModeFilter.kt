package com.bytex.mockportal.web

import org.springframework.web.filter.GenericFilterBean
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Specify deployment mode filter which is used to set MockPortal deployment mode in every response.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class DeploymentModeFilter: GenericFilterBean() {
    private fun doFilter(response: HttpServletResponse) {
        for (profile in environment.activeProfiles) {
            response.addHeader(X_DEPLOYMENT_MODE_RESPONSE_HEADER, profile)
        }
    }

    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        if (request is HttpServletRequest && response is HttpServletResponse)
            doFilter(response)
        chain.doFilter(request, response)
    }
}
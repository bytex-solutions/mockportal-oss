package com.bytex.mockportal.util

import org.springframework.util.CollectionUtils
import org.springframework.util.MultiValueMap
import java.util.*

/**
 * Converts map into map with multiple values associate
 */
fun <K, V> Map<K, V>.toMultiValueMap(): MultiValueMap<K, V> {
    val map = HashMap<K, MutableList<V>>()
    this.forEach { key, value -> if (value != null) map[key] = Collections.singletonList<V>(value) }
    return CollectionUtils.toMultiValueMap(map)
}
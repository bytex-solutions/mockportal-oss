package com.bytex.mockportal.web.security

import com.bytex.mockportal.configuration.Profiles
import com.bytex.mockportal.configuration.hasLdapConfig
import com.bytex.mockportal.web.security.auth.BasicExternalAuthenticationFilter
import com.bytex.mockportal.web.security.auth.ldap.LdapAuthenticationFilter
import com.bytex.mockportal.web.security.auth.ldap.LdapConfiguration
import com.bytex.mockportal.web.security.auth.simple.PredefinedUsers
import com.bytex.mockportal.web.security.auth.simple.SimpleAuthenticationFilter
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

/**
 * Represents security infrastructure implementation for on-premise deployment.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@Profile(Profiles.ON_PREMISE)
@Service
@EnableConfigurationProperties(PredefinedUsers::class, LdapConfiguration::class)
internal class OnPremiseSecurityService(private val localUsers: PredefinedUsers = PredefinedUsers(), private val ldap: LdapConfiguration): WebSecurityService {

    override fun checkClaim(claimType: String, content: String) = WebSecurityService.ClaimCheck.UNRECOGNIZED

    override val hasLoginPage: Boolean = false

    override fun createExternalAuthFilter(loginPathPrefix: String, env: Environment): BasicExternalAuthenticationFilter =
            if (env.hasLdapConfig) {
                val authenticator = ldap.createAuthenticator(env)
                LdapAuthenticationFilter(loginPathPrefix, authenticator, ldap.import)
            } else
                SimpleAuthenticationFilter(loginPathPrefix, localUsers)

}
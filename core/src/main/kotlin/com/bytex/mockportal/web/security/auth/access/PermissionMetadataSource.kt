package com.bytex.mockportal.web.security.auth.access

import org.springframework.core.annotation.AnnotationUtils
import org.springframework.security.access.ConfigAttribute
import org.springframework.security.access.method.AbstractFallbackMethodSecurityMetadataSource
import java.lang.reflect.Method

object PermissionMetadataSource: AbstractFallbackMethodSecurityMetadataSource() {
    override fun getAllConfigAttributes(): MutableCollection<ConfigAttribute>? = null

    override fun findAttributes(method: Method, targetClass: Class<*>) =
            AnnotationUtils.findAnnotation(method, Authorization::class.java)?.permissionSet ?: emptySet()


    override fun findAttributes(clazz: Class<*>) =
            AnnotationUtils.findAnnotation(clazz, Authorization::class.java)?.permissionSet ?: emptySet()
}
package com.bytex.mockportal.web

import com.bytex.mockportal.serviceModel.ApplicationService
import org.springframework.security.oauth2.client.resource.UserRedirectRequiredException
import org.springframework.security.web.RedirectStrategy
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Represents HTTP redirection service.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
interface RedirectionService: ApplicationService, RedirectStrategy {
    fun redirectToMainPage(request: HttpServletRequest, response: HttpServletResponse)
    fun sendRedirect(request: HttpServletRequest, response: HttpServletResponse, redirection: UserRedirectRequiredException)
}

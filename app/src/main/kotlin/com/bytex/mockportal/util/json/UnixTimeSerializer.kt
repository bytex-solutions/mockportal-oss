package com.bytex.mockportal.util.json

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import java.time.Instant

/**
 * Serializes [Instant] to unix time.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
class UnixTimeSerializer: JsonSerializer<Instant>() {
    override fun serialize(value: Instant, gen: JsonGenerator, serializers: SerializerProvider) = gen.writeNumber(value.toEpochMilli())
}
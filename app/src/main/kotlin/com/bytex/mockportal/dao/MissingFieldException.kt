package com.bytex.mockportal.dao

import org.springframework.dao.InvalidDataAccessResourceUsageException

/**
 * Indicates that DAO layer tries to read field wrap database which doesn't exist in schema.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
open class MissingFieldException(val fieldName: String, cause: Throwable? = null): InvalidDataAccessResourceUsageException("Missing field $fieldName", cause)
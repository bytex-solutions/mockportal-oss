package com.bytex.mockportal.web.security

import com.bytex.mockportal.serviceModel.ApplicationService
import com.bytex.mockportal.web.security.auth.ExternalAuthenticationFilter
import org.springframework.core.env.Environment
import org.springframework.security.core.AuthenticationException
import java.security.Principal

/**
 * Represents web security service responsible for authentication and authorization in MockPortal.
 * This service should be implemented for each deployment profile.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
interface WebSecurityService: ApplicationService {
    enum class ClaimCheck {
        SATISFIED,
        UNSATISFIED,
        UNRECOGNIZED
    }

    /**
     * Creates a new external authentication filter user to authenticate MockPortal users with help of external SSO providers.
     * @param loginPathPrefix Login path.
     * @param env Application environment.
     */
    fun createExternalAuthFilter(loginPathPrefix: String, env: Environment): ExternalAuthenticationFilter

    /**
     * Indicates that security infrastructure supports login page.
     */
    val hasLoginPage: Boolean

    /**
     * Checks the custom claim.
     * @param claimType Type of custom claim.
     * @param content Claim content.
     */
    fun checkClaim(claimType: String, content: String): ClaimCheck

    /**
     * Updates content of the claim if necessary or return the same content.
     */
    fun updateClaim(user: Principal, claimType: String, content: String): String = content

    /**
     * Returns path postfix for login page if authentication is failed.
     * @param error Authentication error.
     * @param details Request attribute provider.
     * @return Postfix for login page; or empty string for redirection on main login page.
     */
    fun adviceLoginPagePath(error: AuthenticationException, details: (String) -> Any?): String = ""
}
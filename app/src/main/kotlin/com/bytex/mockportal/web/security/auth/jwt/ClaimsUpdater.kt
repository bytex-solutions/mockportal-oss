package com.bytex.mockportal.web.security.auth.jwt

import java.security.Principal

internal typealias ClaimsUpdater = (Principal, String, String) -> String
package com.bytex.mockportal.web.security

import com.bytex.mockportal.web.security.auth.UserAuthenticationTrustResolver
import com.bytex.mockportal.web.security.auth.access.PermissionDecisionVoter
import com.bytex.mockportal.web.security.auth.access.PermissionMetadataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler
import org.springframework.security.access.expression.method.ExpressionBasedPreInvocationAdvice
import org.springframework.security.access.method.MethodSecurityMetadataSource
import org.springframework.security.access.prepost.PreInvocationAuthorizationAdviceVoter
import org.springframework.security.access.vote.AbstractAccessDecisionManager
import org.springframework.security.access.vote.AffirmativeBased
import org.springframework.security.access.vote.AuthenticatedVoter
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.BeanIds
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration

@EnableGlobalMethodSecurity(prePostEnabled = true)
internal open class PermissionBasedMethodSecurity(@Autowired @Qualifier(BeanIds.AUTHENTICATION_MANAGER) private val authManager: AuthenticationManager): GlobalMethodSecurityConfiguration() {
    final override fun authenticationManager(): AuthenticationManager = authManager

    final override fun customMethodSecurityMetadataSource(): MethodSecurityMetadataSource = PermissionMetadataSource

    private fun createPreInvocationAuthorizationAdviceVoter(): PreInvocationAuthorizationAdviceVoter {
        val expressionAdvice = ExpressionBasedPreInvocationAdvice()
        expressionAdvice.setExpressionHandler(expressionHandler)
        return PreInvocationAuthorizationAdviceVoter(expressionAdvice)
    }

    final override fun createExpressionHandler() =
            DefaultMethodSecurityExpressionHandler().apply { setTrustResolver(UserAuthenticationTrustResolver) }

    private fun createAuthenticatedVoter(): AuthenticatedVoter {
        val authVoter = AuthenticatedVoter()
        authVoter.setAuthenticationTrustResolver(UserAuthenticationTrustResolver)
        return authVoter
    }

    final override fun accessDecisionManager(): AbstractAccessDecisionManager {
        var decisionEngine = super.accessDecisionManager() as? AbstractAccessDecisionManager
        if (decisionEngine == null)
            decisionEngine = AffirmativeBased(listOf(
                    createAuthenticatedVoter(),
                    createPreInvocationAuthorizationAdviceVoter(),
                    PermissionDecisionVoter
            ))
        else {
            //replace authentication voter with correct trust resolver
            decisionEngine.decisionVoters.forEach { (it as? AuthenticatedVoter)?.setAuthenticationTrustResolver(UserAuthenticationTrustResolver) }
            decisionEngine.decisionVoters.add(PermissionDecisionVoter)
        }
        return decisionEngine
    }
}
package com.bytex.mockportal.dao.mongo

import com.bytex.mockportal.util.conversion.Convertible
import com.bytex.mockportal.util.conversion.ConversionFormat
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.stringify
import org.bson.Document
import org.bson.types.Binary

internal fun Convertible.serialize(): Variant = when {
    supportsConversion(ConversionFormat.JSON) -> Variant(Document.parse(convertTo(ConversionFormat.JSON).stringify()))
    supportsConversion(ConversionFormat.PlainText) -> Variant(convertTo(ConversionFormat.PlainText))
    supportsConversion(ConversionFormat.Binary) -> Variant(Binary(convertTo(ConversionFormat.Binary)))
    else -> throw IllegalArgumentException("Object $this doesn't support any known serialization format")
}

internal fun Convertible.deserialize(value: Variant): Boolean = value.value.let {
    when (it) {
        is Binary -> {
            convertFrom(ConversionFormat.Binary, it.data)
            true
        }
        is String -> {
            convertFrom(ConversionFormat.PlainText, it)
            true
        }
        is Document -> {
            convertFrom(ConversionFormat.JSON, ConversionFormat.JSON(it.toJson()))
            true
        }
        else -> false
    }
}

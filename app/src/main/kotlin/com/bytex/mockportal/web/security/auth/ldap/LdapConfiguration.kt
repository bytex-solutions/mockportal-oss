package com.bytex.mockportal.web.security.auth.ldap

import org.springframework.boot.autoconfigure.ldap.LdapProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.NestedConfigurationProperty
import org.springframework.core.env.Environment
import org.springframework.ldap.core.support.LdapContextSource
import org.springframework.security.ldap.authentication.LdapAuthenticator
import java.util.*

/**
 * Represents LDAP configuration.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@ConfigurationProperties("spring.ldap")
internal class LdapConfiguration: LdapProperties(), ActiveDirectorySupport {

    /**
     * Indicates that Active Directory is used as Directory Services
     */
    @NestedConfigurationProperty
    var activeDirectory = true

    /**
     * Represents authentication method
     */
    @NestedConfigurationProperty
    var authenticationMethod = LdapAuthenticationMethod.BIND

    @NestedConfigurationProperty
    val userSearch = UserSearchConfiguration()

    @NestedConfigurationProperty
    val import = ImportSettings()

    override fun setupActiveDirectory() {
        userSearch.setupActiveDirectory()
    }

    private fun buildContextSource(env: Environment) = LdapContextSource().apply {
        userDn = this@LdapConfiguration.username
        password = this@LdapConfiguration.password
        isAnonymousReadOnly = this@LdapConfiguration.anonymousReadOnly
        setBase(this@LdapConfiguration.base)
        urls = this@LdapConfiguration.determineUrls(env)
        setBaseEnvironmentProperties(Collections.unmodifiableMap<String, Any?>(this@LdapConfiguration.baseEnvironment))
    }

    fun createAuthenticator(env: Environment): LdapAuthenticator {
        val source = buildContextSource(env)
        val authenticator = authenticationMethod.createAuthenticator(source)
        if (activeDirectory)
            setupActiveDirectory()
        userSearch.createSearchEngine(source)?.let { authenticator.setUserSearch(it) }
        return authenticator
    }
}
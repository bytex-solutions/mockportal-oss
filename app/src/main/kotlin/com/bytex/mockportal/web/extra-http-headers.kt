package com.bytex.mockportal.web

//Additional HTTP headers

const val X_FORWARDED_HOST_REQUEST_HEADER = "X-Forwarded-Host"

/**
 * Represents deployment mode header.
 */
const val X_DEPLOYMENT_MODE_RESPONSE_HEADER = "X-Deployment-Mode"
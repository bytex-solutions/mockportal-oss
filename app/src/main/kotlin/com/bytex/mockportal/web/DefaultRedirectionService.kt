package com.bytex.mockportal.web

import com.bytex.mockportal.util.toMultiValueMap
import org.springframework.security.oauth2.client.resource.UserRedirectRequiredException
import org.springframework.security.web.DefaultRedirectStrategy
import org.springframework.stereotype.Service
import org.springframework.web.util.UriComponentsBuilder
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Represents HTTP redirection service.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@Service
class DefaultRedirectionService: DefaultRedirectStrategy(), RedirectionService {

    override fun sendRedirect(request: HttpServletRequest, response: HttpServletResponse, redirection: UserRedirectRequiredException) {
        UriComponentsBuilder
                .fromHttpUrl(request.requestURL.toString())
                .replacePath(redirection.redirectUri)
                .replaceQueryParams(redirection.requestParams.toMultiValueMap())
                .toUriString()
                .also { sendRedirect(request, response, it) }
    }

    override fun redirectToMainPage(request: HttpServletRequest, response: HttpServletResponse){
        UriComponentsBuilder
                .fromHttpUrl(request.requestURL.toString())
                .replacePath(null)
                .replaceQuery(null)
                .toUriString()
                .also { sendRedirect(request, response, it) }
    }
}
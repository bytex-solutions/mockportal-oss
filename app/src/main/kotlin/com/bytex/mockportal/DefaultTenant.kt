package com.bytex.mockportal

import com.bytex.mockportal.dao.*
import com.bytex.mockportal.util.declareLogger
import com.bytex.mockportal.util.ifEmpty
import com.bytex.mockportal.util.iterable
import org.springframework.data.util.CloseableIterator
import java.time.Instant
import java.util.*
import kotlin.NoSuchElementException

/**
 * Represents default implementation of tenant.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class DefaultTenant private constructor(queryResult: TenantGateway.GetTenantDataQueryResult,
                                                 private val tenants: TenantGateway,
                                                 private val users: UserGateway): Tenant {
    /**
     * Factory methods for safe access to the users.
     */
    internal companion object {
        private val logger = declareLogger()

        fun take(id: Identifier, tenants: TenantGateway, users: UserGateway): DefaultTenant? {
            val queryResult =
                    try {
                        tenants[id]
                    } catch (e: EntityNotFoundException) {
                        logger.debug("Requested tenant $id doesn't exist", e)
                        return null
                    }
            return DefaultTenant(queryResult, tenants, users)
        }

        fun take(name: String, tenants: TenantGateway, users: UserGateway): DefaultTenant? {
            val queryResult =
                    try {
                        tenants[name]
                    } catch (e: EntityNotFoundException) {
                        logger.debug("Requested tenant $name doesn't exist", e)
                        return null
                    }
            return DefaultTenant(queryResult, tenants, users)
        }
    }

    private val id: Identifier = queryResult.id
    private var deleted = false

    private fun checkDeleted(): DefaultTenant = if (deleted) throw IllegalStateException("Tenant $id was deleted") else this

    override var name: String = queryResult.name
        get() = checkDeleted().run { field }
        set(name) = checkDeleted().run {
            try {
                tenants.updateName(id, name)
            } catch (e: DuplicateEntityException) {
                throw DuplicateTenantNameException(name, e)
            }
            field = name
        }

    /**
     * Display name of tenant.
     */
    override var displayName: String = queryResult.displayName
        get() = checkDeleted().run { field.ifEmpty { name } }
        set(displayName) = checkDeleted().run {
            tenants.updateDisplayName(id, displayName)
            field = displayName
        }

    override fun delete(withUsers: Boolean): Long = checkDeleted().run {
        val numberOfUsers = if (withUsers) users.deleteByTenant(id) else users.detachFromTenant(id)
        tenants.delete(id)
        deleted = true
        numberOfUsers
    }

    override val createdAt: Instant = Instant.ofEpochMilli(queryResult.createdAt)

    private fun iterator(userNames: CloseableIterator<String>) = object : Iterator<DefaultUser> {
        private var current: DefaultUser? = null

        override tailrec fun hasNext(): Boolean = if (userNames.hasNext()) {
            //take user by its login
            current = DefaultUser.take(userNames.next(), users)
            if (current == null) hasNext() else true
        } else {
            userNames.close()
            false
        }

        override fun next(): DefaultUser = current ?: throw NoSuchElementException()
    }

    /**
     * Gets iterator of all users associated with this tenant.
     * @return Iterator over users.
     */
    override fun iterator(): Iterator<DefaultUser> = iterator(users.findAttachedUsers(id).iterator())

    /**
     * Gets users in this tenant by role
     * @param role User role
     * @return Collection of users with the specified role.
     */
    override fun get(role: UserRole): Iterable<DefaultUser> = iterable { iterator(users.findAttachedUsers(id, role.toString()).iterator()) }

    override fun linkUser(user: User) =
        when {
            users.findTenant(user.login) == id -> Unit
            !users.updateTenant(user.login, id) -> throw UserAssociationException(user.login)
            else -> Unit
        }

    override fun unlinkUser(user: User) = users.detachFromTenant(user.login, id)


    override fun Tenant.Metadata.read(): Boolean = tenants.readMetadata(id, namespace, this)

    override fun Tenant.Metadata.write() = tenants.updateMetadata(id, namespace, this)

    override fun hashCode(): Int = Objects.hash(id, tenants, users)
    override fun equals(other: Any?): Boolean = other is DefaultTenant && id == other.id && tenants == other.tenants && users == other.users
    override fun toString(): String = if (deleted) "DELETED TENANT" else displayName
}
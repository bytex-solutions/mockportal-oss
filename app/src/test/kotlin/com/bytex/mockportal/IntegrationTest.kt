package com.bytex.mockportal

import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

/**
 * Abstract class for all integration tests.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration(classes = [MockPortal::class])
abstract class IntegrationTest
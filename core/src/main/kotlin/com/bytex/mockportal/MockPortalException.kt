package com.bytex.mockportal

/**
 * Represents exception associated with application business logic.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
abstract class MockPortalException(override val message: String, cause: Throwable? = null) : Exception(message, cause)
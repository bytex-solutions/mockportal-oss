package com.bytex.mockportal.dao

import org.springframework.dao.DuplicateKeyException

/**
 * Indicates that data entity already exists in the database.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
open class DuplicateEntityException(val id: Any, cause: Throwable?=null): DuplicateKeyException("Entity $id already exists in database", cause)
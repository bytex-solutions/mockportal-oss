package com.bytex.mockportal.web.security.auth

import org.springframework.security.core.Authentication
import java.net.InetSocketAddress
import java.security.cert.X509Certificate

/**
 * Represents authentication of mock request.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
sealed class RequestAuthentication: Authentication {
    /**
     * Unique identifier of tenant.
     */
    abstract override fun getName(): String

    /**
     * Represents request authenticated using IP address.
     */
    abstract class InternetAddress: RequestAuthentication(){
        abstract override fun getCredentials(): InetSocketAddress
    }

    /**
     * Represents request authenticated using X.509 client certificate.
     */
    abstract class ClientCertificate: RequestAuthentication(){
        abstract override fun getCredentials(): X509Certificate
    }
}
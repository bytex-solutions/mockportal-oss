package com.bytex.mockportal.web.security.auth.ldap

/**
 *
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal interface ActiveDirectorySupport {
    fun setupActiveDirectory()
}
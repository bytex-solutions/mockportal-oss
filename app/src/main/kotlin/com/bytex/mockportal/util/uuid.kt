package com.bytex.mockportal.util

import java.nio.ByteBuffer
import java.util.*

internal fun UUID.toByteArray(): ByteArray = ByteBuffer.wrap(ByteArray(16))
            .putLong(this.mostSignificantBits)
            .putLong(this.leastSignificantBits)
            .array()

internal fun parseUUID(bytes: ByteArray): UUID {
    val byteBuffer = ByteBuffer.wrap(bytes)
    return UUID(byteBuffer.long, byteBuffer.long)
}

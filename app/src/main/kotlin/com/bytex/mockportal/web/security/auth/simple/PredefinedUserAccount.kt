package com.bytex.mockportal.web.security.auth.simple

import org.springframework.boot.context.properties.NestedConfigurationProperty
import java.security.MessageDigest
import java.util.*
import java.util.function.Predicate
import javax.xml.bind.annotation.adapters.HexBinaryAdapter

/**
 * Account of user defined in configuration file.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class PredefinedUserAccount: Predicate<String> {
    companion object {
        private val hex = HexBinaryAdapter()
        fun fromHEX(hex: String): ByteArray = Companion.hex.unmarshal(hex)
    }

    @NestedConfigurationProperty
    var displayName = ""

    @NestedConfigurationProperty
    var password = ""

    @NestedConfigurationProperty
    var passwordStorageFormat = ""

    override fun test(password: String): Boolean = when {
        password.isEmpty() || this.password.isEmpty() -> false
        passwordStorageFormat.isEmpty() -> password == this.password
        else -> {
            val actual = MessageDigest.getInstance(passwordStorageFormat).digest(password.toByteArray())
            val expected = fromHEX(this.password)
            actual contentEquals expected
        }
    }

    override fun hashCode(): Int = Objects.hash(displayName, password, passwordStorageFormat)

    override fun equals(other: Any?): Boolean = other is PredefinedUserAccount &&
            password == other.password &&
            displayName == other.displayName &&
            passwordStorageFormat == other.passwordStorageFormat
}
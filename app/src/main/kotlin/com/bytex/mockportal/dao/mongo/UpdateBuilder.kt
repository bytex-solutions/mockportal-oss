package com.bytex.mockportal.dao.mongo

import com.bytex.mockportal.util.toByteArray
import org.bson.Document
import org.bson.conversions.Bson
import org.bson.types.Binary
import org.bson.types.ObjectId
import java.util.*
import java.util.function.Supplier

/**
 * The utility for creating update queries.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class UpdateBuilder: Supplier<Bson> {
    private enum class UpdateOperator(private val value: String) {
        SET("\$set"),
        INC("\$inc"),
        UNSET("\$unset");

        override fun toString(): String = value
    }

    /**
     * The update query.
     */
    private val query = Document()

    private fun addToQuery(operator: String, key: String, value: Any) {
        val subQuery = query[operator] as? Document ?: Document()
        query[operator] = subQuery.append(key, value)
    }

    private fun addToQuery(operator: UpdateOperator, key: String, value: Any) = addToQuery(operator.toString(), key, value)

    infix fun <V : Any> DocumentField<V>.set(value: V) = addToQuery(UpdateOperator.SET, name, value)
    infix fun DocumentField<Binary>.set(value: ByteArray) = set(Binary(value))
    infix fun DocumentField<Binary>.set(value: UUID) = set(Binary(value.toByteArray()))
    infix fun DocumentField<ObjectId>.set(value: MongoIdentifier) = set(value.value)
    infix fun DocumentField<Variant>.set(value: Variant) = addToQuery(UpdateOperator.SET, name, value.value)

    infix fun DocumentField<Long>.inc(value: Long) = addToQuery(UpdateOperator.INC, name, value)

    fun DocumentField<*>.unset() = addToQuery(UpdateOperator.UNSET, name, 1)

    override fun get(): Bson = query

    override fun toString(): String = query.toString()

    override fun equals(other: Any?): Boolean = other is UpdateBuilder && query == other.query

    override fun hashCode(): Int = query.hashCode()
}

internal inline fun update(body: UpdateBuilder.() -> Unit): Bson {
    val builder = UpdateBuilder()
    builder.body()
    return builder.get()
}
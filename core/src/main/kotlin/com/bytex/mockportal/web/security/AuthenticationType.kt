package com.bytex.mockportal.web.security

/**
 * Represents one of supported authentication types.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
enum class AuthenticationType(val isOAuth2: Boolean) {
    /**
     * Google OAuth2 for cloud deployment.
     */
    GOOGLE_OAUTH2(true),
    /**
     * GitHub OAuth2 for cloud deployment.
     */
    GITHUB_OAUTH2(true),
    /**
     * Facebook OAuth2 for cloud deployment.
     */
    FACEBOOK_OAUTH2(true),
    /**
     * Corporate OAuth2 for cloud deployment.
     */
    CORPORATE_OAUTH2(true),
    /**
     * LDAP-based authentication for on-premise deployment.
     */
    LDAP(false),
    /**
     * Simple file-based authentication for on-premise deployment.
     */
    SIMPLE(false);
}
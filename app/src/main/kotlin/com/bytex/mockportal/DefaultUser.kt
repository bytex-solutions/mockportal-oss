package com.bytex.mockportal

import com.bytex.mockportal.dao.EntityNotFoundException
import com.bytex.mockportal.dao.UserGateway
import com.bytex.mockportal.util.declareLogger
import com.bytex.mockportal.util.ifEmpty
import com.bytex.mockportal.web.security.AuthenticationType
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken
import org.springframework.security.oauth2.common.OAuth2RefreshToken
import java.net.URI
import java.time.Instant
import java.util.*

/**
 * Creates a new user.
 * @constructor Initializes a new user domain object.
 * @throws EntityNotFoundException User with the specified login doesn't exist.
 */
internal class DefaultUser private constructor(queryResult: UserGateway.GetUserDataQueryResult,
                           private val users: UserGateway): User {
    internal companion object {
        private val logger = declareLogger()

        fun take(login: String, users: UserGateway): DefaultUser?{
            val queryResult = try{
                users[login]
            } catch (e: EntityNotFoundException){
                logger.debug("Requested user $login doesn't exist", e)
                return null
            }
            return DefaultUser(queryResult, users)
        }
    }

    private var deleted = false
    override val login: String = queryResult.login

    private fun checkDeleted(): DefaultUser = if(deleted) throw IllegalStateException("User is deleted") else this
    /**
     * Represents authentication type.
     */
    override val accountType: AuthenticationType = AuthenticationType.valueOf(queryResult.authenticationType)
        get() = checkDeleted().run { field }

    /**
     * Gets last login of the user in its timezone.
     */
    override var lastLogin: Instant = Instant.ofEpochMilli(queryResult.lastLogin)
        private set(lastLogin) = checkDeleted().run {
            users.updateLoginTime(login, lastLogin.toEpochMilli())
            field = lastLogin
        }

    /**
     * Indicates token that identifies snapshot of security information associated with this user.
     */
    override var passcode: UUID = queryResult.passcode
        get() = checkDeleted().run { field }
        private set

    override val createdAt: Instant = Instant.ofEpochMilli(queryResult.createdAt)

    /**
     * Gets or sets display name of user.
     */
    override var displayName: String = queryResult.displayName
        get() = checkDeleted().run { field.ifEmpty { login } }
        set(displayName) = checkDeleted().run {
            users.updateDisplayName(login, displayName)
            field = displayName
        }

    /**
     * Gets or sets OAuth2 access token associated with this user.
     */
    override var refreshToken: OAuth2RefreshToken? = queryResult.refreshToken.let { if (it.isEmpty()) null else DefaultOAuth2RefreshToken(it) }
        set(refreshToken) = checkDeleted().run {
            users.updateRefreshToken(login, if (refreshToken == null) "" else refreshToken.value)
            field = refreshToken
        }

    /**
     * Indicates that this user is enabled.
     */
    override var enabled: Boolean = queryResult.enabled
        set(enabled) = checkDeleted().run {
            if (enabled != field) {
                users.updateUserStatus(login, enabled)
                passcode = users.updatePasscode(login)
                field = enabled
            }
        }

    /**
     * Reports that login is occured by user.
     */
    override fun loginOccurs() = checkDeleted().run {
        lastLogin = Instant.now()
    }

    /**
     * Gets or sets user role.
     */
    override var role: UserRole = UserRole.valueOf(URI.create(queryResult.role))
        set(role) = checkDeleted().run {
            if (role != field) {
                users.updateRole(login, role.toString())
                passcode = users.updatePasscode(login)
                field = role
            }
        }

    override var email: String = queryResult.email
        get() = checkDeleted().run { field }
        set(email) = checkDeleted().run {
            users.updateEmail(login, email)
            field = email
        }

    /**
     * Reports that logout was initiated by user directly.
     */
    override fun logoutOccurs() = checkDeleted().run {
        //establish new passcode so the next login even with correct token will be failed and redirected to login page
        passcode = users.updatePasscode(login)
        Unit
    }

    override fun leave() = users.detachFromTenant(login)

    override fun delete(){
        users.delete(login)
    }

    override fun hashCode(): Int = Objects.hash(login, users)

    override fun equals(other: Any?): Boolean =
            other is DefaultUser &&
                    login == other.login &&
                    users == other.users

    override fun toString(): String = if(deleted) "DELETED USER" else displayName
}
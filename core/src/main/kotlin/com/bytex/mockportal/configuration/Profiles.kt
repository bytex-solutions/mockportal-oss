package com.bytex.mockportal.configuration

import org.springframework.core.env.Environment

/**
 * Defines additional Spring profiles introduced by MockPortal.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
object Profiles {
    /**
     * Indicates that MockPortal is deployed in cloud.
     */
    const val CLOUD = "cloud"
    /**
     * Indicates that MockPortal is deployed as on-premise software.
     */
    const val ON_PREMISE = "on-premise"

    val Environment.isCloud: Boolean
        get() = acceptsProfiles(CLOUD)

    val Environment.isOnPremise: Boolean
        get() = acceptsProfiles(ON_PREMISE)
}
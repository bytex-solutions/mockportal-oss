package com.bytex.mockportal.dao.mongo

import com.bytex.mockportal.dao.Identifier
import org.bson.types.ObjectId
import java.nio.ByteBuffer

/**
 * Represents Mongo-specific identifier.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal data class MongoIdentifier(val value: ObjectId): Identifier, Comparable<MongoIdentifier> {
    constructor(id: ByteArray): this(ObjectId(id))
    constructor(id: ByteBuffer): this(ObjectId(id))

    override fun compareTo(other: MongoIdentifier): Int = value.compareTo(other.value)

    override fun toString(): String = value.toString()
}
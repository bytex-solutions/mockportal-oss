package com.bytex.mockportal.web.security.auth

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.GenericFilterBean
import java.net.InetSocketAddress
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

/**
 * Overrides [org.springframework.security.web.authentication.AnonymousAuthenticationFilter].
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class AnonymousAuthenticationFilter: GenericFilterBean() {
    private class AnonymousUserAuthentication(request: HttpServletRequest): UserAuthentication.Anonymous() {
        private val clientAddress = InetSocketAddress.createUnresolved(request.remoteHost, request.remotePort)
        private val userName = request.remoteUser

        override fun getName(): String = userName ?: "anonymous"

        override fun getCredentials(): InetSocketAddress = clientAddress

        override fun getDetails(): Any? = null

        override fun toString(): String = name

        override fun hashCode(): Int = Objects.hash(clientAddress, userName)

        override fun equals(other: Any?): Boolean =
                other is AnonymousUserAuthentication && clientAddress == other.clientAddress && userName == other.userName
    }

    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        if (request is HttpServletRequest && request.userPrincipal == null)
            SecurityContextHolder.getContext().authentication = AnonymousUserAuthentication(request)
        chain.doFilter(request, response)
    }
}
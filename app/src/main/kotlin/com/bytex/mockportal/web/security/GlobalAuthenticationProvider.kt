package com.bytex.mockportal.web.security

import com.bytex.mockportal.Permission
import com.bytex.mockportal.User
import com.bytex.mockportal.UserManager
import com.bytex.mockportal.UserRole
import com.bytex.mockportal.web.security.auth.InvalidAccountTypeException
import com.bytex.mockportal.web.security.auth.RequestAuthentication
import com.bytex.mockportal.web.security.auth.UserAuthentication
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.ProviderManager
import org.springframework.security.config.BeanIds
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import kotlin.reflect.KClass

/**
 * Represents abstract authentication provider.
 */
private sealed class AbstractAuthenticationProvider<in A: Authentication>(private val authType: KClass<A>): AuthenticationProvider {
    final override fun supports(authentication: Class<*>?): Boolean = authType.java.isAssignableFrom(authentication)

    protected abstract fun authenticateCore(authentication: A): Authentication

    final override fun authenticate(authentication: Authentication?): Authentication? = if (authType.isInstance(authentication)) authenticateCore(authType.java.cast(authentication)) else null
}

private class ExternalSuccessfulAuthentication(private val user: User, source: UserAuthentication.External) : UserAuthentication.Successful(source) {
    override fun getPrincipal(): User = user
}

private class InternalSuccessfulAuthentication(private val users: UserManager, source: UserAuthentication.Internal): UserAuthentication.Successful(source) {
    override fun getAuthorities(): Set<Permission> = source.authorities

    override fun getName(): String = source.name

    override fun getPrincipal(): User = users[name]
            ?: throw UsernameNotFoundException("User $name was deleted")
}

/**
 * Represents user authentication provider.
 */
private class UserAuthenticationProvider(context: ApplicationContext): AbstractAuthenticationProvider<UserAuthentication>(UserAuthentication::class) {
    private val users: UserManager = context.getBean(UserManager::class.java)

    private fun authenticateExternal(authentication: UserAuthentication.External): UserAuthentication.Successful = (users[authentication.name]
            ?: users.create(authentication.name, UserRole.TENANT_ADMIN, authentication.type)).let {
        when {
        //the same user name but with different authentication type. This means that authentication should be aborted.
            it.accountType != authentication.type -> throw InvalidAccountTypeException(authentication.name)
        //check if user enabled
            !it.enabled -> throw DisabledException("User account ${authentication.name} is disabled")
            else -> {
                authentication.updateUserProfile(it)
                it.loginOccurs()
                return ExternalSuccessfulAuthentication(it, authentication)
            }
        }
    }

    private fun authenticateInternal(authentication: UserAuthentication.Internal): UserAuthentication.Successful = if (users.isValidPasscode(authentication.name, authentication.passcode))
        InternalSuccessfulAuthentication(users, authentication)
    else
        throw BadCredentialsException("Invalid passcode. User access rights were modified")

    override fun authenticateCore(authentication: UserAuthentication): UserAuthentication = when (authentication) {
        is UserAuthentication.External -> authenticateExternal(authentication)
        is UserAuthentication.Internal -> authenticateInternal(authentication)
        is UserAuthentication.Successful, is UserAuthentication.Anonymous -> authentication
    }
}

private class RequestAuthenticationProvider(context: ApplicationContext): AbstractAuthenticationProvider<RequestAuthentication>(RequestAuthentication::class){
    override fun authenticateCore(authentication: RequestAuthentication): Authentication {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

/**
 * Represents global authentication provider
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@Service(BeanIds.AUTHENTICATION_MANAGER)
internal class GlobalAuthenticationProvider(@Autowired context: ApplicationContext): ProviderManager(listOf(RequestAuthenticationProvider(context), UserAuthenticationProvider(context)))
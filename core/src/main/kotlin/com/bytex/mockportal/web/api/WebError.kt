package com.bytex.mockportal.web.api

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Represents JSON-serializable representation of error returned by REST API.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
open class WebError(@JsonProperty("code") val errorCode: String,
                    @JsonProperty("msg") val message: String,
                    @JsonProperty("rec") val recoverable: Boolean = true) {
    constructor(e: Throwable) : this(e.javaClass.simpleName, e.message ?: "", e is Exception)
}
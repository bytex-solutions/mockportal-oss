package com.bytex.mockportal.util.json

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.time.Instant

/**
 * Deserializes [Instant] from unix time.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
class UnixTimeDeserializer: JsonDeserializer<Instant>() {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Instant = Instant.ofEpochMilli(p.longValue)
}
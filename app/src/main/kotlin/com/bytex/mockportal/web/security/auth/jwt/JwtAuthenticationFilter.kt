package com.bytex.mockportal.web.security.auth.jwt

import org.springframework.context.ApplicationContext
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.InternalAuthenticationServiceException
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import java.lang.ref.WeakReference
import javax.annotation.PreDestroy
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse



/**
 * Represents JWT-based authentication filter.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class JwtAuthenticationFilter(securedPath: String, context: ApplicationContext): AbstractAuthenticationProcessingFilter(securedPath), AutoCloseable {
    private companion object {
        /**
         * Represents servlet request attribute that can be used to store login across different filters.
         */
        const val LOGIN_ATTR = "login"
    }

    private val appContext = WeakReference(context)

    private fun throwFilterClosed(): Nothing = throw IllegalStateException("Filter is closed")

    private val jwts: JwtService = context.getBean(JwtService::class.java) ?: throwFilterClosed()

    var tokenRenewalRate: Float = 0.5F

    init {
        allowSessionCreation = false
    }

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        var authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION) ?: throw MissingTokenException()
        authorizationHeader = authorizationHeader.removePrefix("Bearer ")
        val token = try {
            jwts.decodeAndVerify(authorizationHeader)
        } catch (e: Throwable) {
            throw BadTokenException(authorizationHeader, e)
        }
        //read claims
        val jwt = token.createUserInternalAuthentication()
        val appContext = this.appContext.get()
                ?: throw InternalAuthenticationServiceException("Application context is not available")
        request.setAttribute(LOGIN_ATTR, jwt.subject)
        //check all claims
        for (claim in jwt)
            if (!claim.isSatisfied(appContext))
                throw claim.prepareException()
        return authenticationManager.authenticate(jwt)
    }

    override fun successfulAuthentication(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain, authResult: Authentication) {
        super.successfulAuthentication(request, response, chain, authResult)
        chain.doFilter(request, response)
    }

    @PreDestroy
    override fun close() {
        appContext.clear()
    }
}
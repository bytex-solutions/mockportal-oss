package com.bytex.mockportal.web.security.auth

import com.bytex.mockportal.configuration.Profiles.isCloud
import com.bytex.mockportal.web.security.AuthenticationType
import com.bytex.mockportal.web.security.WebSecurityService
import org.springframework.context.ApplicationContext
import org.springframework.security.authentication.AuthenticationServiceException
import org.springframework.security.authentication.CredentialsExpiredException
import org.springframework.security.authentication.InternalAuthenticationServiceException
import org.springframework.security.core.AuthenticationException
import org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException
import java.time.Instant
import java.util.*

/**
 * Represents claim.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
sealed class Claim(val name: String) {
    /**
     * Indicates that this claim is satisfied.
     * @param context Application context
     * @return true, if this claim is satisfied; otherwise, false.
     */
    abstract fun isSatisfied(context: ApplicationContext): Boolean

    abstract fun prepareException(): AuthenticationException

    /**
     * Represents expiration claim
     */
    data class Expiration(val expiredAt: Instant): Claim("expiration") {
        override fun isSatisfied(context: ApplicationContext): Boolean = expiredAt >= Instant.now()

        override fun prepareException(): CredentialsExpiredException = CredentialsExpiredException("Credentials was expired at $expiredAt")

        override fun hashCode(): Int = Objects.hash(javaClass, expiredAt)
        override fun equals(other: Any?): Boolean = other is Expiration && expiredAt == other.expiredAt
        override fun toString(): String = "Expired at $expiredAt"
    }

    data class AuthType(val type: AuthenticationType): Claim("authType") {
        override fun isSatisfied(context: ApplicationContext): Boolean =
                type.isOAuth2 == context.environment.isCloud

        override fun prepareException(): AuthenticationException = UnapprovedClientAuthenticationException("Authentication type $type is not supported")
        override fun toString(): String = type.toString()
        override fun equals(other: Any?): Boolean = other is AuthType && type == other.type
        override fun hashCode(): Int = Objects.hash(javaClass, type)
    }

    /**
     * Represents custom claim.
     * @constructor Constructs a new custom claim with the specified name and content.
     */
    class Custom(name: String, val content: String): Claim(name) {
        class ClaimNotSatisfiedException(val claim: Custom): AuthenticationServiceException("Cannot satisfy claim ${claim.name}")
        class ClaimNotSupportedException(val claim: Custom): InternalAuthenticationServiceException("Claim ${claim.name} is not supported")

        /**
         * Indicates that this claim is satisfied.
         * @param context Application context
         * @return true, if this claim is satisfied; otherwise, false.
         */
        override fun isSatisfied(context: ApplicationContext): Boolean {
            val securityService = context.getBean(WebSecurityService::class.java)
            return when (securityService.checkClaim(name, content)) {
                WebSecurityService.ClaimCheck.SATISFIED -> true
                WebSecurityService.ClaimCheck.UNSATISFIED -> false
                WebSecurityService.ClaimCheck.UNRECOGNIZED -> throw  ClaimNotSupportedException(this)
            }
        }

        override fun prepareException(): ClaimNotSatisfiedException = ClaimNotSatisfiedException(this)

        override fun equals(other: Any?): Boolean = other is Custom && name == other.name && content == other.content

        override fun hashCode(): Int = Objects.hash(javaClass, name, content)

        override fun toString(): String = "$name = $content"
    }
}
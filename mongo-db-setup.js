//Collection of users
db.users.createIndex({login: 1}, {unique: true})

//Collection of tenants
db.tenants.createIndex({name: 1}, {unique: true})
db.tenants.createIndex({users: 1})
db.tenants.createIndex({owner: 1}, {unique: true})

package com.bytex.mockportal.util.conversion

import org.springframework.core.serializer.support.SerializationFailedException

/**
 * Special interface for all objects which can be serialized and deserialized using different serialization forms.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
interface Convertible {
    /**
     * Checks whether this object supports specified serialization format.
     * @param format Serialization format.
     * @return true, if serialization format is supported.
     */
    fun supportsConversion(format: ConversionFormat<*>): Boolean

    /**
     * Serializes object into the specified serialization format.
     * @param format Serialization format.
     * @return Serialized form.
     * @throws SerializationFailedException Serialization format is not supported.
     */
    fun <F : Any> convertTo(format: ConversionFormat<F>): F

    /**
     * Restores state of this object from serialized form.
     * @param format Serialization format.
     * @param input Serialized form.
     * @throws SerializationFailedException Serialization format is not supported.
     */
    fun <F : Any> convertFrom(format: ConversionFormat<F>, input: F)
}
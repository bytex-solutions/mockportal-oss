package com.bytex.mockportal.web.security.auth

import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.authentication.AuthenticationTrustResolver
import org.springframework.security.core.Authentication

/**
 * Represents trust resolver for user authentications.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
object UserAuthenticationTrustResolver: AuthenticationTrustResolver {
    override fun isRememberMe(authentication: Authentication?): Boolean = false

    override fun isAnonymous(authentication: Authentication?): Boolean =
            authentication is UserAuthentication.Anonymous || authentication is AnonymousAuthenticationToken
}
package com.bytex.mockportal.dao

import org.springframework.dao.TypeMismatchDataAccessException
import kotlin.reflect.KClass

/**
 * Indicates that field has incorrect type.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
open class UnexpectedDataTypeException(val fieldName: String, val expectedType: KClass<*>, val actualType: KClass<*>):
        TypeMismatchDataAccessException("Field $fieldName has incorrect type in database. Expected ${expectedType.qualifiedName} but found ${actualType.qualifiedName}")
package com.bytex.mockportal.web.security.auth

import com.bytex.mockportal.UserAlreadyExistsException
import org.springframework.security.core.AuthenticationException

/**
 * Indicates that user cannot be authenticated.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
class InvalidAccountTypeException(login: String): AuthenticationException("User $login already exists in the system and have different authentication method", UserAlreadyExistsException(login)){
    override fun fillInStackTrace(): Throwable = this
}
package com.bytex.mockportal.web.security.auth.ldap

import com.bytex.mockportal.User
import com.bytex.mockportal.UserRole
import com.bytex.mockportal.web.security.AuthenticationType
import com.bytex.mockportal.web.security.auth.BasicExternalAuthenticationFilter
import com.bytex.mockportal.web.security.auth.UserAuthentication
import org.springframework.ldap.core.DirContextOperations
import org.springframework.security.ldap.authentication.LdapAuthenticator

/**
 * Represents authentication filter based on LDAP authentication.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class LdapAuthenticationFilter(loginPathPrefix: String, private val authenticator: LdapAuthenticator, private val import: ImportSettings) : BasicExternalAuthenticationFilter(loginPathPrefix) {
    private class LdapAuthentication(private val context: DirContextOperations, private val credentials: String, private val import: ImportSettings): UserAuthentication.External(){
        override val type: AuthenticationType = AuthenticationType.LDAP

        override fun updateUserProfile(user: User) {
            //update display name
            context.getStringAttribute(import.displayNameAttribute)?.let { user.displayName = it }
            //try to resolve role
            context.getStringAttribute(import.roleAttribute)?.let {
                user.role = UserRole.valueOf(it)
            }
            for (groupName in context.getStringAttributes("memberOf").orEmpty())
                import.roleMapping[groupName]?.let { user.role = it }
        }

        override fun getName(): String = context.nameInNamespace

        override fun getCredentials() = credentials
        
        override fun getPrincipal(): Any = this

        override fun getDetails() = context
    }

    override fun attemptAuthentication(token: BasicAuthToken): UserAuthentication.External {
        val context = authenticator.authenticate(token)
        return LdapAuthentication(context, token.credentials, import)
    }
}
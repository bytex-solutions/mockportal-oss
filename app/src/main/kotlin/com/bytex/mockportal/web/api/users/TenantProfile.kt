package com.bytex.mockportal.web.api.users

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Represents tenant profile.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal data class TenantProfile(@JsonProperty var name: String, @JsonProperty var displayName: String)
package com.bytex.mockportal.web.security.auth.access

import com.bytex.mockportal.Permission
import com.bytex.mockportal.web.security.auth.UserAuthentication
import org.springframework.security.access.AccessDecisionVoter
import org.springframework.security.access.ConfigAttribute
import org.springframework.security.core.Authentication
import java.util.stream.Collectors

/**
 * Represents decision voter based on permissions.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
object PermissionDecisionVoter: AccessDecisionVoter<Any> {
    private fun filterAttributes(attributes: Collection<ConfigAttribute>): Set<Permission> =
            attributes.stream().filter { it is Permission }.map { it as Permission }.collect(Collectors.toSet())

    private fun vote(authentication: UserAuthentication, permissions: Set<Permission>) = when{
        permissions.isEmpty() -> AccessDecisionVoter.ACCESS_ABSTAIN  //method is not declared any required permissions
        authentication.authorities.containsAll(permissions) -> AccessDecisionVoter.ACCESS_GRANTED
        else -> AccessDecisionVoter.ACCESS_DENIED
    }

    override fun vote(authentication: Authentication, securedObj: Any?, attributes: MutableCollection<ConfigAttribute>) =
            if (authentication is UserAuthentication) vote(authentication, filterAttributes(attributes)) else AccessDecisionVoter.ACCESS_ABSTAIN

    override fun supports(attribute: ConfigAttribute?): Boolean = attribute is Permission

    override fun supports(clazz: Class<*>?): Boolean = true
}
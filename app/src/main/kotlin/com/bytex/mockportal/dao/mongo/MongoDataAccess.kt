package com.bytex.mockportal.dao.mongo

import com.bytex.mockportal.configuration.MONGO_DATABASE_NAME_PROPERTY
import com.bytex.mockportal.dao.TenantGateway
import com.bytex.mockportal.dao.UserGateway
import com.mongodb.MongoClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.cache.CacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Collection of users
 */
internal const val USERS_COLLECTION = "users"

/**
 * Collection of tenants
 */
internal const val TENANTS_COLLECTION = "tenants"

/**
 * Describes data access components based on Mongo implementation.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@Configuration
internal open class MongoDataAccess(@Autowired client: MongoClient, @Value("\${$MONGO_DATABASE_NAME_PROPERTY}") database: String) {
    private val database = client.getDatabase(database)

    @Bean
    open fun createUserGateway(@Autowired cache: CacheManager): UserGateway =
            MongoUserGateway(database.getCollection(USERS_COLLECTION), cache).apply { loadPasscodes() }

    @Bean
    open fun createTenantGateway(): TenantGateway = MongoTenantGateway(database.getCollection(TENANTS_COLLECTION))
}
package com.bytex.mockportal.web.security

import com.bytex.mockportal.configuration.JWT_COOKIE_PROPERTY
import com.bytex.mockportal.configuration.JWT_LIFETIME_PROPERTY
import com.bytex.mockportal.configuration.JWT_RENEWAL_RATE
import com.bytex.mockportal.configuration.SERVER_HOST_PROPERTY
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.stringify
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.toJSON
import com.bytex.mockportal.web.DeploymentModeFilter
import com.bytex.mockportal.web.LOGIN_PAGE
import com.bytex.mockportal.web.RedirectionService
import com.bytex.mockportal.web.WEB_API_PATH_PREFIX
import com.bytex.mockportal.web.api.WebError
import com.bytex.mockportal.web.security.auth.*
import com.bytex.mockportal.web.security.auth.jwt.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.CredentialsExpiredException
import org.springframework.security.config.BeanIds
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.AuthenticationFailureHandler
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import org.springframework.security.web.context.SecurityContextPersistenceFilter
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.time.Duration
import javax.annotation.PostConstruct
import javax.servlet.Filter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


/**
 * Base class for Web security configuration.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@EnableWebSecurity
internal open class WebSecurityConfiguration(@Autowired private val jwts: JwtService,
                                             @Autowired private val security: WebSecurityService,
                                             @Autowired private val redirection: RedirectionService,
                                             @Autowired @Qualifier(BeanIds.AUTHENTICATION_MANAGER) private val authManager: AuthenticationManager): WebSecurityConfigurerAdapter(), WebMvcConfigurer, AuthenticationFailureHandler {
    private companion object WebSecurityContextHolder: SecurityContextHolder(), AuthenticationSuccessHandler {
        override fun onAuthenticationSuccess(request: HttpServletRequest, response: HttpServletResponse, authentication: Authentication) {
            getContext().authentication = authentication
        }
    }

    @Value("#{T(java.time.Duration).parse('\${$JWT_LIFETIME_PROPERTY}')}")
    var tokenLifetime: Duration = DEFAULT_TOKEN_TTL

    /**
     * Gets or sets name of JWT cookie.
     */
    @Value("\${$JWT_COOKIE_PROPERTY}")
    var tokenCookie: String = "mpjwt"

    @Value("\${$JWT_RENEWAL_RATE:0.5}")
    var tokenRenewalRate: Float = 0.5F

    @Value("\${$SERVER_HOST_PROPERTY:}")
    var restrictedHostName: String = ""

    @PostConstruct
    fun init() {
        setTrustResolver(UserAuthenticationTrustResolver)
    }

    /**
     * Gets authentication manager constructed by security infrastructure.
     */
    final override fun authenticationManager(): AuthenticationManager = authManager

    final override fun authenticationManagerBean(): AuthenticationManager = authManager

    private fun Filter.prepareFilter(filterName: String) = applicationContext.autowireCapableBeanFactory.apply {
        autowireBean(this@prepareFilter)
        initializeBean(this@prepareFilter, filterName)
    }

    private fun JwtClaims.emitJwtAsCookie(request: HttpServletRequest, response: HttpServletResponse) {
        refresh(security::updateClaim, tokenLifetime)
        emitJwtAsCookie(request, response, jwts, tokenCookie, restrictedHostName)
    }

    private val JwtClaims.isRefreshRequired: Boolean
        get() = isRefreshRequired(tokenRenewalRate)

    private fun emitJwtAsCookie(request: HttpServletRequest, response: HttpServletResponse, authResult: Authentication) {
        authResult is UserAuthentication.Successful && authResult.applyToSourceIf<JwtClaims>({ isRefreshRequired }) {
            emitJwtAsCookie(request, response)
            authResult.principal.loginOccurs()  //reports about new login when new token is issued (user for correct stat)
        }
    }

    private fun createRequestAuthFilter(securedPath: String): JwtAuthenticationFilter = JwtAuthenticationFilter(securedPath, applicationContext).apply {
        setAuthenticationManager(authenticationManager())
        tokenRenewalRate = this@WebSecurityConfiguration.tokenRenewalRate
        setAuthenticationSuccessHandler(this@WebSecurityConfiguration::emitJwtAsCookie)
        setAuthenticationFailureHandler(this@WebSecurityConfiguration)
        prepareFilter("requestAuthFilter")
    }

    private fun turnExternalIntoInternal(request: HttpServletRequest, response: HttpServletResponse, authenticated: Authentication) {
        //user successfully authenticated. Emit JWT, save it into cookie and return to main page
        if (authenticated is UserAuthentication.Successful) {
            val token = JwtClaims(authenticated.principal)
            token.timeToLive = tokenLifetime
            authenticated.applyToSourceIf<UserAuthentication.External>(applier = { exportClaims(token::importClaim) })
            token.emitJwtAsCookie(request, response)
            redirection.redirectToMainPage(request, response)
        }
    }

    private fun createInternalAuthFilter(): InternalAuthenticationFilter = InternalAuthenticationFilter().apply {
        setAuthenticationManager(authenticationManager())
        setAuthenticationSuccessHandler(this@WebSecurityConfiguration::turnExternalIntoInternal)
        setAuthenticationFailureHandler(this@WebSecurityConfiguration)
        prepareFilter("internalAuthFilter")
    }

    private fun createDeploymentModeFilter(): DeploymentModeFilter = DeploymentModeFilter().apply {
        prepareFilter("deploymentModeFilter")
    }

    private fun createExternalAuthFilter(): ExternalAuthenticationFilter = security.createExternalAuthFilter(LOGIN_PAGE, applicationContext.environment).apply {
        setAuthenticationSuccessHandler(WebSecurityContextHolder)
        setAuthenticationFailureHandler(this@WebSecurityConfiguration)
        setAuthenticationManager(authenticationManager())
        prepareFilter(filterName)
    }

    private fun createAnonymousAuthFilter(): AnonymousAuthenticationFilter = AnonymousAuthenticationFilter().apply {
        prepareFilter("anonymousAuthFilter")
    }

    final override fun configure(http: HttpSecurity) {
        //remove all unnecessary filters
        http.anonymous().disable()
        http.csrf().disable()
        http.httpBasic().disable()
        //http.openidLogin().disable()
        //http.oauth2Login().disable()
        http.rememberMe().disable()
        http.formLogin().disable()
        http.requestCache().disable()
        http.logout().disable()
        //construct custom filter chain (respecting native ordering)
        /*1*/http.addFilterBefore(createDeploymentModeFilter(), SecurityContextPersistenceFilter::class.java)
        /*2*/http.addFilterAt(createRequestAuthFilter("$WEB_API_PATH_PREFIX/**"), AbstractPreAuthenticatedProcessingFilter::class.java)
        /*3*/http.addFilterAt(createExternalAuthFilter(), UsernamePasswordAuthenticationFilter::class.java)
        /*4*/http.addFilterAt(createInternalAuthFilter(), BasicAuthenticationFilter::class.java)
        /*5*/http.addFilterAt(createAnonymousAuthFilter(), org.springframework.security.web.authentication.AnonymousAuthenticationFilter::class.java)
        //login page is accessible without authentication
        http.authorizeRequests().antMatchers("$LOGIN_PAGE/**", "/*", "/ui/**").permitAll()
        //REST API of portal requires JWT-based authentication
        http.authorizeRequests().antMatchers("$WEB_API_PATH_PREFIX/**").authenticated()
        //other settings such as login page redirection
        http.exceptionHandling().authenticationEntryPoint(this::onAuthenticationFailure)
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

    final override fun addViewControllers(registry: ViewControllerRegistry) {
        if (security.hasLoginPage)
            registry.addViewController(LOGIN_PAGE).setViewName("forward:/login.html")
    }

    override fun onAuthenticationFailure(request: HttpServletRequest, response: HttpServletResponse, error: AuthenticationException) {
        val webError = when (error) {
            is CredentialsExpiredException, is Claim.Custom.ClaimNotSatisfiedException -> {
                val path = LOGIN_PAGE + security.adviceLoginPagePath(error, request::getAttribute)
                response.setHeader(HttpHeaders.LOCATION, path)
                WebError("unauthenticated", "Credentials are expired")
            }
            is BadCredentialsException -> WebError("unauthenticated", "Incorrect user name or password")
            else -> WebError(error)
        }
        response.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        response.sendError(HttpStatus.UNAUTHORIZED.value(), webError.toJSON().stringify())
    }
}
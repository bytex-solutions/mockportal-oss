package com.bytex.mockportal.dao.mongo

import com.bytex.mockportal.dao.*
import com.bytex.mockportal.util.ifNull
import com.bytex.mockportal.util.parseUUID
import com.mongodb.DuplicateKeyException
import com.mongodb.client.MongoCollection
import org.bson.Document
import org.springframework.cache.CacheManager
import java.util.*

/**
 * User gateway backed by Mongo database.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class MongoUserGateway(users: MongoCollection<Document>, cache: CacheManager): MongoGateway(users), UserGateway {
    object LoginField : StringField("login")
    private object RefreshTokenField : StringField("refreshToken")
    private object AccountTypeField : StringField("accountType")
    private object DisplayNameField : StringField("displayName")
    private object LastLoginField : LongField("lastLogin")
    private object EnabledField : BooleanField("enabled")
    private object PasscodeField : BinaryField("passcode")
    private object RoleField : StringField("role")
    private object CreatedAtField : LongField("createdAt")
    private object EMailField: StringField("email")
    object TenantField : ObjectIdField("tenant")

    private val users get() = collection
    private val passcodes = cache.getCache("mockPortalPassCodes")!!

    private fun newPasscode(): UUID = UUID.randomUUID()

    fun loadPasscodes() {
        //pre-load all passcodes from database
        users.find().selectFields(PasscodeField, LoginField).iterator().use {
            while (it.hasNext()) {
                val next = it.next()
                passcodes.put(next extract LoginField, parseUUID(next extract PasscodeField))
            }
        }
    }

    override tailrec fun checkPasscode(login: String, expected: UUID): Boolean {
        var actual = passcodes.get(login, UUID::class.java)
        return if (actual == null) {  //no cached passcode, load from database
            actual = users.find(filter { LoginField eq login })
                    .selectFields(PasscodeField)
                    .limit(1)
                    .first()
                    .ifNull { EntityNotFoundException(login) }
                    .let { parseUUID(it extract PasscodeField) }
            passcodes.putIfAbsent(login, actual)
            checkPasscode(login, expected)
        } else
            expected == actual
    }

    /**
     * Creates a new user in database.
     * @throws com.bytex.mockportal.dao.DuplicateEntityException User with specified login already exists in database.
     */
    override fun create(login: String, role: String, accountType: String) {
        val passcode = newPasscode()
        try {
            val currentTime = System.currentTimeMillis()
            users.insertOne(document {
                this[LoginField] = login
                this[AccountTypeField] = accountType
                this[RefreshTokenField] = ""
                this[DisplayNameField] = ""
                this[EnabledField] = true
                this[LastLoginField] = currentTime
                this[PasscodeField] = passcode
                this[RoleField] = role
                this[CreatedAtField] = currentTime
                this[EMailField] = ""
            })
        } catch (e: DuplicateKeyException) {
            throw DuplicateEntityException(login, e)
        }
        passcodes.put(login, passcode)
    }

    private inline fun updateByLogin(login: String, advancedFilter: FilterDocument.() -> Unit, update: UpdateBuilder.() -> Unit) {
        if (users.updateOne(filter { LoginField eq login; advancedFilter() }, UpdateBuilder().apply(update).get()).matchedCount == 0L) {
            throw EntityNotFoundException(login)
        }
    }

    private inline fun updateByLogin(login: String, update: UpdateBuilder.() -> Unit) = updateByLogin(login, {}, update)

    /**
     * Updates OAuth2 refresh token for the specified user.
     * @param login User login.
     * @param refreshToken OAuth2 refresh token in serializable format.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    override fun updateRefreshToken(login: String, refreshToken: String) = updateByLogin(login) { RefreshTokenField set refreshToken }

    /**
     * Updates display name of the specified user.
     * @param login User login.
     * @param displayName Display name of the user, e.g. first name and last name.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    override fun updateDisplayName(login: String, displayName: String) = updateByLogin(login) { DisplayNameField set displayName }


    override fun updateEmail(login: String, email: String) = updateByLogin(login) { EMailField set email }

    /**
     * Updates last login time for the specified user.
     * @param login User login.
     * @param timeStamp Unix time stamp.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    override fun updateLoginTime(login: String, timeStamp: Long) = updateByLogin(login) { LastLoginField set timeStamp }

    /**
     * Updates status of the user account.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    override fun updateUserStatus(login: String, enabled: Boolean) = updateByLogin(login) {
        EnabledField set enabled
    }

    /**
     * Updates passcode of the user.
     * @param login User login.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    override fun updatePasscode(login: String): UUID {
        val passcode = newPasscode()
        updateByLogin(login, { PasscodeField set passcode })
        passcodes.put(login, passcode)
        return passcode
    }

    /**
     * Retrieves user data by its login.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    override fun get(login: String) = users.find(filter { LoginField eq login })
            .selectFields(AccountTypeField, DisplayNameField, LastLoginField, RefreshTokenField, EnabledField, PasscodeField, RoleField, CreatedAtField, EMailField)
            .limit(1)
            .first()
            .ifNull { EntityNotFoundException(login) }
            .let {
                UserGateway.GetUserDataQueryResult(authenticationType = it extract AccountTypeField,
                        displayName = it extract DisplayNameField,
                        lastLogin = it extract LastLoginField,
                        refreshToken = it extract RefreshTokenField,
                        enabled = it extract EnabledField,
                        passcode = parseUUID(it extract PasscodeField),
                        role = it extract RoleField,
                        createdAt = it extract CreatedAtField,
                        login = login,
                        email = it extract EMailField)
            }

    /**
     * Updates user role.
     * @param login User login.
     * @param role User role.
     * @throws EntityNotFoundException User with specified login doesn't exist.
     */
    override fun updateRole(login: String, role: String) = updateByLogin(login) {
        RoleField set role
    }

    /**
     * Gets tenant identifier associated with user.
     * @throws EntityNotFoundException
     */
    override fun findTenant(login: String): MongoIdentifier? = users.find(filter { LoginField eq login })
            .selectFields(TenantField)
            .limit(1)
            .first()
            .ifNull { EntityNotFoundException(login) }
            .let {
                try {
                    it extract TenantField
                } catch (e: MissingFieldException) {
                    null
                }
            }

    private fun updateTenant(login: String, tenantId: MongoIdentifier) =
            users.updateOne(filter { LoginField eq login; TenantField exists false }, update { TenantField set tenantId }).matchedCount > 0L

    /**
     * Assigns tenant to the user.
     * @param login Login of the user.
     * @param tenantId Tenant identifier.
     */
    override fun updateTenant(login: String, tenantId: Identifier) = updateTenant(login, tenantId as MongoIdentifier)

    private fun detachFromTenant(tenantId: MongoIdentifier) =
            users.updateMany(filter { TenantField eq tenantId }, update { TenantField.unset() }).matchedCount


    override fun detachFromTenant(tenantId: Identifier): Long = detachFromTenant(tenantId as MongoIdentifier)

    private fun deleteByTenant(tenantId: MongoIdentifier): Long =
            users.deleteMany(filter { TenantField eq tenantId }).deletedCount

    override fun deleteByTenant(tenantId: Identifier): Long = deleteByTenant(tenantId as MongoIdentifier)

    private fun detachFromTenant(login: String) =
            users.updateOne(filter { LoginField eq login }, update { TenantField.unset() }).matchedCount > 0

    private fun detachFromTenant(login: String, tenantId: MongoIdentifier) =
            users.updateOne(filter { LoginField eq login; TenantField eq tenantId }, update { TenantField.unset() }).matchedCount > 0

    override fun detachFromTenant(login: String, tenantId: Identifier?) =
            if (tenantId == null) detachFromTenant(login) else detachFromTenant(login, tenantId as MongoIdentifier)

    override fun delete(login: String): Boolean {
        val result = users.deleteOne(filter { LoginField eq login }).deletedCount > 0
        passcodes.evict(login)   //remove cached passcode
        return result
    }


    private fun findAttachedUsers(tenantId: MongoIdentifier, role: String): DataSequence<String> =
            if (role.isEmpty())
                users.find(filter { TenantField eq tenantId }).selectFields(LoginField).map { it extract LoginField }.wrap()
            else
                users.find(filter { TenantField eq tenantId; RoleField eq role }).selectFields(LoginField).map { it extract LoginField }.wrap()


    override fun findAttachedUsers(tenantId: Identifier, role: String): DataSequence<String> = findAttachedUsers(tenantId as MongoIdentifier, role)
}
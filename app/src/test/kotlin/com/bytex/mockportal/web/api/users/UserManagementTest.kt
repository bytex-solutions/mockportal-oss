package com.bytex.mockportal.web.api.users

import com.bytex.mockportal.UserRole
import com.bytex.mockportal.configuration.Profiles
import com.bytex.mockportal.util.conversion.ConversionFormat.JSON.toJSON
import com.bytex.mockportal.web.WEB_API_PATH_PREFIX
import com.bytex.mockportal.web.WebTest
import org.junit.Test
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import kotlin.test.assertEquals

/**
 * Represents integrations tests for user management api.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@ActiveProfiles(Profiles.ON_PREMISE)
internal class UserManagementTest: WebTest() {
    @Test
    fun userNameTest() {
        var response = restClient.getForEntity("$WEB_API_PATH_PREFIX/user/", UserProfile::class.java)
        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals(testUser.name, response.body?.login)
        assertEquals(UserRole.TENANT_ADMIN, response.body?.role)
        //upgrade display name
        restClient.put("$WEB_API_PATH_PREFIX/user/displayName", "Frank Underwood".toJSON())
        response = restClient.getForEntity("$WEB_API_PATH_PREFIX/user/", UserProfile::class.java)
        assertEquals("Frank Underwood", response.body?.displayName)
    }
}
package com.bytex.mockportal.util

import org.slf4j.LoggerFactory

internal inline fun <reified T: Any> T.declareLogger() = LoggerFactory.getLogger(this::class.qualifiedName)

package com.bytex.mockportal.web.security.auth

import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Represents external authentication mechanism.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
abstract class ExternalAuthenticationFilter(private val filterName: String, protected val requestMatcher: AntPathRequestMatcher): AbstractAuthenticationProcessingFilter(requestMatcher) {
   init {
       allowSessionCreation = false
   }

    protected constructor(filterName: String, uriTemplate: String) : this(filterName, AntPathRequestMatcher(uriTemplate))

    final override fun successfulAuthentication(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain, authResult: Authentication?) {
        if (authResult is UserAuthentication.External) {
            super.successfulAuthentication(request, response, chain, authResult)
            chain.doFilter(request, response)
        }
    }

    abstract override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): UserAuthentication.External?

    public final override fun getFilterName(): String = filterName
}
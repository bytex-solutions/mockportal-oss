package com.bytex.mockportal.util

import java.time.Duration
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReadWriteLock

inline fun <R>Lock.lock(timeout: Duration? = null, action: () -> R): R {
    if (timeout == null)
        lockInterruptibly()
    else if (!tryLock(timeout.toMillis(), TimeUnit.MILLISECONDS))
        throw TimeoutException()
    try {
        return action()
    } finally {
        unlock()
    }
}

inline fun <R>ReadWriteLock.readLock(timeout: Duration? = null, action: () -> R): R = readLock().lock(timeout, action)
inline fun <R>ReadWriteLock.writeLock(timeout: Duration? = null, action: () -> R): R = writeLock().lock(timeout, action)

@file:JvmName("MockPortalBootstrapper")

package com.bytex.mockportal

import org.springframework.boot.runApplication

fun main(args: Array<String>) {
    runApplication<MockPortal>(*args)
}
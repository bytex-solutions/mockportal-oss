package com.bytex.mockportal

import org.springframework.security.access.ConfigAttribute
import org.springframework.security.core.GrantedAuthority
import java.net.URI

/**
 * Represents permission.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
enum class Permission(resource: String, action: String): GrantedAuthority, ConfigAttribute {
    RELEASE_MANAGEMENT("release", "management"),
    TENANT_MANAGEMENT("tenant", "management"),
    EXECUTE_MOCK_REQUESTS("mock", "execute");

    /**
     * Represents permission in the form of URN.
     */
    val urn = URI("urn:com:bytex:mockportal:security:permission:$resource:$action")

    override fun getAuthority(): String = urn.toString()

    override fun toString(): String = authority

    override fun getAttribute(): String = authority
}
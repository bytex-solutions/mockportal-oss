package com.bytex.mockportal.web.security.auth.simple

import com.bytex.mockportal.configuration.PREDEFINED_USERS_PROPERTY
import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Represents set of users defined in local file.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@ConfigurationProperties(PREDEFINED_USERS_PROPERTY)
internal class PredefinedUsers: HashMap<String, PredefinedUserAccount>()
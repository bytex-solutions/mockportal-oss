package com.bytex.mockportal.web

/**
 * Prefix of API page.
 */
const val WEB_API_PATH_PREFIX = "/api"

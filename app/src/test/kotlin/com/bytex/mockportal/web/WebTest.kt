package com.bytex.mockportal.web

import com.bytex.mockportal.IntegrationTest
import com.bytex.mockportal.configuration.JWT_COOKIE_PROPERTY
import com.bytex.mockportal.web.security.auth.BasicExternalAuthenticationFilter.BasicAuthToken
import com.bytex.mockportal.web.security.auth.UserAuthentication
import com.bytex.mockportal.web.security.auth.jwt.JwtService
import com.bytex.mockportal.web.security.auth.jwt.createUserInternalAuthentication
import org.junit.Rule
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.client.RootUriTemplateHandler
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpRequest
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.http.client.ClientHttpResponse
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.http.client.support.BasicAuthorizationInterceptor
import org.springframework.security.jwt.Jwt
import org.springframework.web.client.RestTemplate
import java.net.HttpCookie
import java.net.HttpURLConnection
import kotlin.test.fail

/**
 * Represents abstract class for all REST API integration tests.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
internal abstract class WebTest(userName: String = "admin", password: String = "pwd", followRedirect: Boolean = false): IntegrationTest(), ApplicationContextAware {
    private class TestRequestFactory(private val followRedirect: Boolean): SimpleClientHttpRequestFactory(){
        override fun prepareConnection(connection: HttpURLConnection, httpMethod: String) {
            super.prepareConnection(connection, httpMethod)
            connection.instanceFollowRedirects = followRedirect
        }
    }

    /**
     * Represents authentication rule.
     */
    interface TestAuthentication: TestRule {
        /**
         * Result of authentication
         */
        val result: UserAuthentication.Internal
    }

    private class DefaultTestAuthentication(private val restClient: RestTemplate,
                                            private val tokenCookieName: String,
                                            private val jwts: JwtService): Statement(), TestAuthentication, ClientHttpRequestInterceptor {
        private lateinit var token: Jwt

        init {
            restClient.interceptors.add(this)
        }

        override fun apply(base: Statement, description: Description) = object : Statement() {
            override fun evaluate() {
                this@DefaultTestAuthentication.evaluate()
                base.evaluate()
            }
        }

        override fun evaluate() {
            val response = restClient.getForEntity(LOGIN_PAGE, String::class.java)
            for (cookie in response.headers[HttpHeaders.SET_COOKIE] ?: fail("Cookie was not found"))
                for (@Suppress("NAME_SHADOWING") cookie in HttpCookie.parse(cookie))
                    if (cookie.name == tokenCookieName) {
                        token = jwts.decodeAndVerify(cookie.value)
                        return
                    }
            fail("JWT cookie $tokenCookieName was not found")
        }

        override val result: UserAuthentication.Internal by lazy { token.createUserInternalAuthentication() }

        override fun intercept(request: HttpRequest, body: ByteArray, execution: ClientHttpRequestExecution): ClientHttpResponse {
            if (this::token.isInitialized)
                request.headers[HttpHeaders.AUTHORIZATION] = "Bearer ${token.encoded}"
            return execution.execute(request, body)
        }
    }

    /**
     * Credentials of test user.
     */
    protected val testUser = BasicAuthToken(userName, password)

    /**
     * REST client for tests.
     */
    protected val restClient: RestTemplate = RestTemplate(TestRequestFactory(followRedirect)).apply {
        interceptors.add(BasicAuthorizationInterceptor(testUser.name, testUser.credentials))
    }

    private lateinit var _authentication: TestAuthentication

    /**
     * Gets authentication result.
     */
    val authentication: TestAuthentication
        @Rule get() = _authentication

    final override fun setApplicationContext(context: ApplicationContext) {
        //initialize base URI for rest client
        val serverPort = context.environment.getProperty("local.server.port")
        RootUriTemplateHandler.addTo(restClient, "http://localhost:$serverPort/")
        //initialize authentication rule
        val tokenCookieName = context.environment.getProperty(JWT_COOKIE_PROPERTY)
                ?: fail("Token name is not configured")
        val jwts = context.getBean(JwtService::class.java)
        _authentication = DefaultTestAuthentication(restClient, tokenCookieName, jwts)
    }
}
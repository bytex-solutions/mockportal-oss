package com.bytex.mockportal

import com.bytex.mockportal.dao.DuplicateEntityException
import com.bytex.mockportal.dao.TenantGateway
import com.bytex.mockportal.dao.UserGateway
import com.bytex.mockportal.util.declareLogger
import com.bytex.mockportal.util.ifNotNull
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Represents default implementation of tenant manager.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@Service
internal class DefaultTenantManager(@Autowired private val tenants: TenantGateway,
                                    @Autowired private val users: UserGateway): TenantManager {
    private val log = declareLogger()

    override operator fun get(name: String): DefaultTenant? {
        log.debug("Request tenant by name $name")
        return DefaultTenant.take(name, tenants, users)
    }

    override val User.tenant: DefaultTenant?
        get() = users.findTenant(login)?.let { DefaultTenant.take(it, tenants, users) }

    /**
     * Creates a new tenant and assign the specified user as tenant owner.
     * @param name Short name of tenant
     * @param owner Owner of tenant.
     */
    override fun create(name: String, owner: User): DefaultTenant =
            users.findTenant(owner.login).ifNotNull({ UserAssociationException(owner.login) }) {
                log.debug("Attempt to create tenant $name by user ${owner.login}")
                //create tenant
                val id = try {
                    tenants.create(name)
                } catch (e: DuplicateEntityException) {
                    log.debug("Failed to create tenant $name by user ${owner.login}", e)
                    throw TenantAlreadyExistsException(name, e)
                }
                if (users.updateTenant(owner.login, id)) { //associate user with tenant
                    log.debug("Tenant $name is created successfully by user ${owner.login}")
                    DefaultTenant.take(id, tenants, users)
                            ?: throw TenantCreationException(name, "Tenant $name was accidentally deleted during creation")
                } else { //user already has a tenant. Rollback creation.
                    tenants.delete(id)
                    throw UserAssociationException(owner.login)
                }
            }
}
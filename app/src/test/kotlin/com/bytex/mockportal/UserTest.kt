package com.bytex.mockportal

import com.bytex.mockportal.configuration.Profiles
import com.bytex.mockportal.web.security.AuthenticationType
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import kotlin.test.*

/**
 *
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@ActiveProfiles(Profiles.ON_PREMISE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
internal class UserTest: IntegrationTest() {
    @Autowired
    private lateinit var users: UserManager

    @Test
    fun userCreationTest() {
        assertNotNull(users.create("barry", UserRole.TENANT_ADMIN, AuthenticationType.SIMPLE).apply { email = "barry@acme.com" })
        var user = users["barry"] ?: fail("User is null")
        assertTrue(user.enabled)
        assertNull(user.refreshToken)
        assertTrue(users.isValidPasscode(user.login, user.passcode))
        assertEquals("barry", user.displayName)
        user.displayName = "Barry Burton"
        user.role = UserRole.QA_ENGINEER
        user.enabled = false
        user = users["barry"] ?: fail("User is null")
        assertEquals("Barry Burton", user.displayName)
        //logout user and re-generate passcode
        val oldPasscode = user.passcode
        assertTrue(users.isValidPasscode(user.login, oldPasscode))
        user.logoutOccurs()
        assertFalse(users.isValidPasscode(user.login, oldPasscode))
        assertTrue(users.isValidPasscode(user.login, user.passcode))
        assertEquals("barry@acme.com", user.email)
    }
}
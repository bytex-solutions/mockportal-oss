package com.bytex.mockportal

/**
 *
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
class UserAlreadyExistsException(login: String, cause: Throwable? = null): UserException(login, "User $login already registered", cause)
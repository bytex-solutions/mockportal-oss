package com.bytex.mockportal.web.security.auth.access

import com.bytex.mockportal.Permission

/**
 * Declares required permission for REST API method
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@MustBeDocumented
annotation class Authorization(val requiredPermission: Permission, vararg val additionalPermissions: Permission)

val Authorization.permissionSet
    get()= setOf(requiredPermission, *additionalPermissions)
package com.bytex.mockportal

/**
 * Indicates that tenant cannot be created.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
class TenantCreationException(name: String, message: String, cause: Throwable? = null) : TenantException(name, message, cause)
package com.bytex.mockportal.web.security.auth.ldap

import org.springframework.ldap.core.support.BaseLdapPathContextSource
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch
import org.springframework.security.ldap.search.LdapUserSearch

/**
 * Represents configuration of user search.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class UserSearchConfiguration: ActiveDirectorySupport {
    var searchBase = ""
    var searchFilter = ""
    var searchSubtree = true

    fun createSearchEngine(source: BaseLdapPathContextSource): LdapUserSearch? =
            if(searchBase.isNotEmpty() && searchFilter.isNotEmpty())
                FilterBasedLdapUserSearch(searchBase, searchFilter, source).apply { setSearchSubtree(this@UserSearchConfiguration.searchSubtree)  }
            else
                null

    override fun setupActiveDirectory() {
        searchBase = "DC=company,DC=com"
        searchFilter = "(sAMAccountName={0})"
        searchSubtree = true
    }
}
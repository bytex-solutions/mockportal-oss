package com.bytex.mockportal

import com.hazelcast.core.HazelcastInstance
import com.hazelcast.core.IMap
import com.hazelcast.nio.ObjectDataInput
import com.hazelcast.nio.ObjectDataOutput
import com.hazelcast.nio.serialization.DataSerializable
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.ldap.LdapAutoConfiguration
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientAutoConfiguration
import org.springframework.cache.Cache
import org.springframework.cache.CacheManager
import org.springframework.context.annotation.Bean
import java.util.*
import java.util.concurrent.Callable
import java.util.function.Supplier
import java.util.stream.Collectors


/**
 * Represents mock portal
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@SpringBootApplication(exclude = [LdapAutoConfiguration::class, OAuth2ClientAutoConfiguration::class])  //we provide our own configuration for LDAP
internal open class MockPortal {
    private class HazelcastCachedValue(`val`: Any?) : Cache.ValueWrapper, Supplier<Any?>, DataSerializable {
        /**
         * Special public constructor required for correct deserialization
         */
        @Suppress("unused") constructor() : this(null)

        var value: Any? = `val`
            private set

        override fun get(): Any? = value

        val className: String = value?.javaClass?.name ?: "null"

        infix fun instanceOf(type: Class<*>): Boolean = type.isInstance(value)

        infix fun <T> cast(type: Class<T>): T = type.cast(value)

        override fun writeData(out: ObjectDataOutput) {
            out.writeObject(value)
        }

        override fun readData(`in`: ObjectDataInput) {
            value = `in`.readObject()
        }

        override fun toString(): String = Objects.toString(value)

        override fun hashCode(): Int = Objects.hashCode(value)

        override fun equals(other: Any?): Boolean = other is HazelcastCachedValue && Objects.equals(value, other.value)
    }

    private class HazelcastCache(private val map: IMap<Any, HazelcastCachedValue>) : Cache {
        override fun clear() {
            map.clear()
        }

        override fun put(key: Any, value: Any?) {
            map[key] = HazelcastCachedValue(value)
        }

        override fun getName(): String = map.name

        override fun getNativeCache(): IMap<Any, HazelcastCachedValue> = map

        override operator fun get(key: Any): HazelcastCachedValue? = map[key]

        @Suppress("UNCHECKED_CAST")
        override operator fun <T : Any?> get(key: Any, type: Class<T>?): T? {
            val valueHolder = this[key]
            return when {
                valueHolder == null -> null
                type == null -> valueHolder.value as T?
                valueHolder.value == null -> null
                valueHolder instanceOf type -> valueHolder cast type
                else -> throw IllegalStateException("Incorrect requested type. Expected ${type.name} but found ${valueHolder.className}")
            }
        }

        @Suppress("UNCHECKED_CAST")
        override fun <T : Any?> get(key: Any, valueLoader: Callable<T?>): T? {
            var wrappedValue = this[key]
            return if (wrappedValue == null) {
                map.lock(key)
                try {
                    wrappedValue = this[key]
                    if (wrappedValue == null) {
                        val result = valueLoader.call()
                        map[key] = HazelcastCachedValue(result)
                        result
                    } else
                        wrappedValue.get() as T?
                } finally {
                    map.unlock(key)
                }
            } else
                wrappedValue.get() as T?
        }

        override fun evict(key: Any) {
            map.remove(key)
        }

        override fun putIfAbsent(key: Any, value: Any?): HazelcastCachedValue? = map.putIfAbsent(key, HazelcastCachedValue(value))

        override fun toString(): String = name

        override fun hashCode(): Int = System.identityHashCode(map)

        override fun equals(other: Any?): Boolean = other is HazelcastCache && map === other.map
    }

    private class HazelcastCacheManager(private val hazelcast: HazelcastInstance) : CacheManager {
        override fun getCacheNames(): MutableCollection<String> = hazelcast.distributedObjects.stream()
                .filter { it is IMap<*, *> }
                .map { it.name }
                .collect(Collectors.toSet())


        override fun getCache(name: String): HazelcastCache = HazelcastCache(hazelcast.getMap(name))
    }

    @Bean("cacheManager")
    open fun createCacheManager(@Autowired hazelcast: HazelcastInstance): CacheManager = HazelcastCacheManager(hazelcast)
}
package com.bytex.mockportal.dao

import org.springframework.dao.DataRetrievalFailureException

/**
 * Indicates that requested entity doesn't exist in database.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
open class EntityNotFoundException(val id: Any, cause: Throwable? = null): DataRetrievalFailureException("Entity $id doesn't exist in database", cause)
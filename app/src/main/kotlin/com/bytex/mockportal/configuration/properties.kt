package com.bytex.mockportal.configuration

import org.springframework.core.env.Environment

internal const val MONGO_DATABASE_NAME_PROPERTY = "spring.data.mongodb.database"
internal const val JWT_SECRET_PROPERTY = "security.jwt.secret"
internal const val JWT_LIFETIME_PROPERTY = "security.jwt.lifetime"
internal const val JWT_COOKIE_PROPERTY = "security.jwt.cookie"
internal const val JWT_RENEWAL_RATE = "security.jwt.renewalRate"
internal const val SERVER_HOST_PROPERTY = "server.host"
internal const val PREDEFINED_USERS_PROPERTY = "security.users"
internal const val LDAP_CONFIG_PROPERTY = "security.ldap"

val Environment.hasLdapConfig: Boolean
    get() = containsProperty(LDAP_CONFIG_PROPERTY)
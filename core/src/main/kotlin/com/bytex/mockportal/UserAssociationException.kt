package com.bytex.mockportal

/**
 * Indicates that user is already associated with the tenant.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
class UserAssociationException(name: String, cause: Throwable? = null): UserException(name, "User $name is already a member of another tenant")
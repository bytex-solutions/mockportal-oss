package com.bytex.mockportal.web.security.auth.jwt

import com.bytex.mockportal.web.security.auth.UserAuthentication
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 *
 * Converts authentication object created by [com.bytex.mockportal.web.security.auth.ExternalAuthenticationFilter]
 * into JWT token and save this token into response.
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
internal class InternalAuthenticationFilter: AbstractAuthenticationProcessingFilter("/**") {

    init {
        allowSessionCreation = false
    }

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication =
            authenticationManager.authenticate(SecurityContextHolder.getContext().authentication as UserAuthentication.External)


    override fun requiresAuthentication(request: HttpServletRequest, response: HttpServletResponse) =
            SecurityContextHolder.getContext().authentication is UserAuthentication.External
}
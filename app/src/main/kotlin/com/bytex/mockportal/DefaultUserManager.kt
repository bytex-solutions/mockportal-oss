package com.bytex.mockportal

import com.bytex.mockportal.dao.DuplicateEntityException
import com.bytex.mockportal.dao.TenantGateway
import com.bytex.mockportal.dao.UserGateway
import com.bytex.mockportal.util.declareLogger
import com.bytex.mockportal.web.security.AuthenticationType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

/**
 * Represents default implementation of user manager
 * @author Roman Sakno
 * @since 1.0
 * @version 1.0
 */
@Service
internal class DefaultUserManager(@Autowired private val users: UserGateway): UserManager {

    private val log = declareLogger()

    /**
     * Indicates that passcode is in white list.
     */
    override fun isValidPasscode(login: String, passcode: UUID): Boolean = users.checkPasscode(login, passcode)

    /**
     * Creates a new user.
     */
    override fun create(login: String, role: UserRole, accountType: AuthenticationType): DefaultUser {
        try {
            users.create(login, role.toString(), accountType.name)
        } catch (e: DuplicateEntityException) {
            log.debug("User $login already exists")
            throw UserAlreadyExistsException(login, e)
        }
        log.debug("User $login is created")
        return DefaultUser.take(login, users)!!
    }

    /**
     * Gets user by its login.
     */
    override fun get(login: String): DefaultUser? {
        log.debug("Request user $login")
        return DefaultUser.take(login, users)
    }
}